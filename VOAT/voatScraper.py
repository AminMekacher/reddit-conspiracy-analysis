# Module for #sad project

import pandas as pd
import pandas_bokeh 
import geopandas as gpd
from bokeh.plotting import figure
import matplotlib.pyplot as plt

import os
import json
from datetime import datetime, timedelta, date
from collections import Counter, defaultdict
import random
import functools, operator
import numpy as np
import datetime as dt
import time, math
from itertools import combinations
from wordcloud import WordCloud, STOPWORDS

from bs4 import BeautifulSoup as bs
import requests
from requests import ReadTimeout, ConnectTimeout, HTTPError, Timeout, ConnectionError

import pycountry
import pycountry_convert as pc
import plotly.express as px
import plotly.graph_objects as go

import networkx as nx
import re
import ast

import tweepy
import community
import pycountry
from geopy.geocoders import Nominatim

from webbot import Browser
from wordcloud import WordCloud, STOPWORDS

from expressvpn import wrapper

def voat_community_analysis(voatCredentials):
    
    '''
    Method used to retrieve a list of the communities (called "subverses") present on Voat, along with their description and the number of subscribers
    '''
    
    web = Browser()
    voatCommunities = []
    
    web.go_to('https://voat.co/account/login?returnUrl=/v/GreatAwakening')
    web.type(voatCredentials['username'] , into='UserName')
    web.type(voatCredentials['password'], into='Password')
    
    web.click('Log in', tag='input')
    web.click('subscriptions', tag='a')
    web.click('Discover', tag='a')
    
    voatContent = web.get_page_source()
    voatSoup = bs(voatContent, features='lxml')
    
    while len(voatSoup.find_all('a', string='next >')) > 0:
        
        communityList = voatSoup.find_all('div', {'class' : 'col-md-6'})
        voatCommunities.extend([{'name': community.find('a', {'class' : 'h4'}).text.split(':')[0], 
                                 'description': community.find('p', {'class' : 'subversedescriptionboxcontent'}).text,
                                 'followers': community.find('span').text.split(' ')[0]} for community in communityList])
        
        web.click('next >', tag='a')
        
        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')
        
        if web.get_title() in ['Play Pen Broken', 'Play Pen Improvements'] : # The webbot has reached an error page; the method returns a DataFrame with the acquired data
            communityDF = pd.DataFrame(voatCommunities)
            return communityDF
    
def voat_data_scraping(twitterAPI, voatCredentials, communityList, psawAPI, queryWord=None, querySource=None, commentsAnalysis=False, userAnalysis=False):
    
    '''
    Use Selenium to login to VOAT. Returns the resulting webpage
    '''
    
    web = Browser()
    
    if commentsAnalysis:
        voatSubs, voatComms = [], []
    else:
        voatSubs = []
    
    # Account Login into Voat
    web.go_to('https://voat.co/account/login?returnUrl=/v/GreatAwakening')
    web.type(voatCredentials['username'] , into='UserName')
    web.type(voatCredentials['password'], into='Password')
    
    web.click('Log in', tag='input')
    
    for community in communityList:
        print("New comm: ", community)
        time.sleep(60)
        communityLink = 'https://voat.co/v/' + community + '/new'
        web.go_to(communityLink)

        time.sleep(5)

        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')

        while len(voatSoup.find_all('a', string='next >')) > 0:
            currentURL = web.get_current_url()
            submissionList = voatSoup.find_all('div', {'class' : 'submission'})
            
            submissionPage = [{'title': submission.find('a', {'class' : 'title'})['title'], 'author': submission.find('a', {'class' : 'author'})['data-username'] 
                               if submission.find('a', {'class' : 'author'}).has_attr('data-username') else 'Unknown',
                               'upvotes': submission['data-ups'], 'downvotes': submission['data-downs'], 'id': submission['data-submission-id'],
                               'community': community,
                               'date': datetime.strptime(submission.find('time')['title'], '%m/%d/%Y %I:%M:%S %p').date().strftime('%Y-%m-%d'),
                               'time': datetime.strptime(submission.find('time')['title'], '%m/%d/%Y %I:%M:%S %p').time().strftime('%H:%M:%S'),
                               'source': submission.find('span', {'class' : 'domain'}).text.replace('(', '').replace(')', ''),
                               'link': submission.find('a', {'class' : 'title'})['href']}
                               for submission in submissionList]
            
            # If a query has been given in the function call, the submissions are filtered beforehand
            if querySource is not None:
                submissionPage = [submission for submission in submissionPage if querySource == submission['source']]
                
            if queryWord is not None:
                submissionPage = [submission for submission in submissionPage if any(query in submission['title'].lower() for query in queryWord)]
                
            voatSubs.extend(submissionPage)

            if commentsAnalysis:
                for query in submissionPage:
                    queryHref = '/v/' + community + '/' + query['id']
                    queryButton = voatSoup.find('a', {'class' : 'comments', 'href' : queryHref})
                    if queryButton is not None and queryButton.text != 'discuss':
                        web.click(queryButton.text, tag='a')

                        # COMMENTS ANALYSIS
                        commentContent = web.get_page_source()
                        commentSoup = bs(commentContent, features='lxml')

                        commentList = commentSoup.find_all('div', {'class' : 'comment'})
                        commentPage = [{'author': comment.find('a', {'class' : 'userinfo'})['data-username']
                                        if comment.find('a', {'class' : 'userinfo'}) else 'None', 
                                        'upvotes': comment['data-ups'], 
                                        'downvotes': comment['data-downs'], 'community': community, 'id': query['id'], 
                                        'body': comment.find('div', {'id': 'commentContent-' + comment['data-comment-id']}).text.replace('\n', '')
                                         if comment.find('div', {'id': 'commentContent-' + comment['data-comment-id']}) else 'None',
                                        'date': datetime.strptime(comment.find('time')['title'], '%m/%d/%Y %I:%M:%S %p').date().strftime('%Y-%m-%d'),
                                        'time': datetime.strptime(comment.find('time')['title'], '%m/%d/%Y %I:%M:%S %p').time().strftime('%H:%M:%S')}
                                        for comment in commentList]

                        voatComms.extend(commentPage)
                        web.go_to(currentURL)

            web.click('next >', tag='a')
            time.sleep(5)

            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')

            if web.get_title() in ['Play Pen Broken', 'Play Pen Improvements'] : # The webbot has reached an error page; the method returns a DataFrame with the acquired data
                print("LEN: ", len(voatSubs))
                            
                continue

    if querySource == 'twitter.com':
        for submission in voatSubs:
            try:
                tweet = twitterAPI.get_status(submission['link'].split('.com/')[1].split('/')[-1], tweet_mode='extended')
                tweetUser = twitterAPI.get_user(tweet.user.screen_name)
                submission['author'] = tweetUser.screen_name
                submission['username'] = tweetUser.name
                submission['followers'] = int(tweetUser.followers_count)
                tweetAccountAge = tweetUser.created_at
                try:
                    submission['account_date'] = tweetAccountAge.strftime("%Y-%m-%d")
                    submission['account_time'] = tweetAccountAge.strftime("%H:%M:%S")
                except ValueError:
                    submission['account_date'] = 'Unknown'
                    submission['account_time'] = 'Unknown'
                    pass

                submission['text'] = tweet.full_text
                tweetDateTime = tweet.created_at

                try:
                    submission['tweet_date'] = tweetDateTime.strftime("%Y-%m-%d")
                    submission['tweet_time'] = tweetDateTime.strftime("%H:%M:%S")
                except ValueError:
                    submission['tweet_date'] = 'Unknown'
                    submission['tweet_time'] = 'Unknown'
                    pass

            except tweepy.TweepError:
                pass
            
    elif querySource == 'youtube.com':
        if querySource == 'youtube.com':
            for submission in voatSubs:
                youtubeRequest = requests.get(submission['link'])
                youtubeSoup = bs(youtubeRequest.text, 'html.parser')
   
                submission['youtube_title'] = youtubeSoup.find('meta', attrs={'itemprop' : 'name'})
                if submission['youtube_title'] is not None:
                    submission['youtube_title'] = submission['youtube_title']['content']

                submission['channel'] = youtubeSoup.find('script', attrs={'type' : 'application/ld+json'})
                if submission['channel'] is not None:
                    submission['channel'] = submission['channel'].text.split('"name":')[1].split('\n')[0].strip().replace('"', '')

                if all(submissionInfo is not None for submissionInfo in [submission['youtube_title'], submission['channel']]):
                    submission['view_count'] = int(float(youtubeSoup.find('div', {'class' : 'watch-view-count'}).text.split(' ')[0].replace(',', '')))
                    submission['likes'] = youtubeSoup.find('button', {'title' : 'I like this'}).text
                    submission['dislikes'] = youtubeSoup.find('button', {'title' : 'I dislike this'}).text
    

    if commentsAnalysis:
        subsDF = pd.DataFrame(voatSubs)
        commsDF = pd.DataFrame(voatComms)
        
        if userAnalysis:
            voatUsers = list(set(list(subsDF.author.values)))
            voatUsers.extend(list(set(list(commsDF.author.values))))
            voat_user_activity(voatUsers, psawAPI)
            
        return subsDF, commsDF
    else:
        subsDF = pd.DataFrame(voatSubs)
        
        if userAnalysis:
            voatUsers = list(set(list(subsDF.author.values)))
            voat_user_activity(voatUsers, psawAPI)
            
        return subsDF
    
    
def voat_user_activity(voatUsers, psawAPI):
    
    '''
    By using the users who posted or commented on Voat, retrieve their activity on Reddit and create a network, where each node is a subreddit. Two subreddits are
    connected if they have been visited by the same user
    '''
    
    print('--- Starting User Analysis')
    redditConspiracyUsers = [user for user in voatUsers if 
                         any(dictLength > 0 for dictLength in [len(psawAPI.redditor_subreddit_activity(user)['comment']), 
                                                               len(psawAPI.redditor_subreddit_activity(user)['submission'])])]
    
    print('--- STARTING GRAPH')
    voatGraph = nx.Graph()
        
    for user in redditConspiracyUsers:
        userActivity = psawAPI.redditor_subreddit_activity(user)
            
        # Submission analysis
        for subreddit, subCount in userActivity['submission'].items():
            if subreddit not in voatGraph.nodes():
                voatGraph.add_node(subreddit, submission_count=subCount, comment_count=0)
            else:
                voatGraph.nodes[subreddit]['submission_count'] += subCount
                    
        # Submission edge 
        for subreddit1, subreddit2 in combinations(list(userActivity['submission'].keys()), 2):
            if subreddit1 != subreddit2:
                if not voatGraph.has_edge(subreddit1, subreddit2):
                    voatGraph.add_edge(subreddit1, subreddit2, weight=1)
                else:
                    voatGraph[subreddit1][subreddit2]['weight'] += 1
                        
        # Comment analysis
        for subreddit, commCount in userActivity['comment'].items():
            if subreddit not in voatGraph.nodes():
                voatGraph.add_node(subreddit, submission_count=0, comment_count=commCount)
            else:
                voatGraph.nodes[subreddit]['comment_count'] += commCount
                    
        # Comment edge
        for subreddit1, subreddit2 in combinations(list(userActivity['comment'].keys()), 2):
            if subreddit1 != subreddit2:
                if not voatGraph.has_edge(subreddit1, subreddit2):
                    voatGraph.add_edge(subreddit1, subreddit2, weight=1)
                else:
                    voatGraph[subreddit1][subreddit2]['weight'] += 1
                    
    # Edge filtering
    
    filteredEdges = [edge for edge in voatGraph.edges(data=True) if edge[2]['weight'] < 1]
    voatGraph.remove_edges_from(filteredEdges)
    
    nodesFiltered = [node for (node, degree) in voatGraph.degree if degree == 0]
    voatGraph.remove_nodes_from(nodesFiltered)
                    
    # Community detection
    
    partition = community.best_partition(voatGraph)
    nx.set_node_attributes(voatGraph, name='community', values=partition)
    
    # Save graph
    
    nx.write_gexf(voatGraph, 'Gephi Files/voatGraph.gexf')
    
    return voatGraph
    
def subverse_wordcloud(voatCredentials, subverse):
    
    '''
    Retrieve the title of the submissions for a specified subverse, and create a word cloud with the text
    '''
    
    web = Browser()
    titleString = ''
    stopwords = set(STOPWORDS)
    
    # Account Login into Voat
    web.go_to('https://voat.co/account/login?returnUrl=/v/GreatAwakening')
    web.type(voatCredentials['username'] , into='UserName')
    web.type(voatCredentials['password'], into='Password')
    
    web.click('Log in', tag='input')
    
    time.sleep(5)
    communityLink = 'https://voat.co/v/' + subverse + '/new'
    web.go_to(communityLink)

    time.sleep(5)

    voatContent = web.get_page_source()
    voatSoup = bs(voatContent, features='lxml')

    while len(voatSoup.find_all('a', string='next >')) > 0:
        currentURL = web.get_current_url()
        submissionList = voatSoup.find_all('div', {'class' : 'submission'})
            
        titlePage = ' '.join([submission.find('a', {'class' : 'title'})['title'].lower() for submission in submissionList])
        titleString += titlePage
        
        web.click('next >', tag='a')
        time.sleep(5)

        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')

        if web.get_title() in ['Play Pen Broken', 'Play Pen Improvements'] : # The webbot has reached an error page; the method returns a DataFrame with the acquired data
            break

           
    titleString = re.sub('[^a-zA-Z0-9 \n]', ' ', titleString)
    titleString = ' '.join(titleString.split())
    
    wordcloud = WordCloud(width = 800, height = 800, 
                background_color ='white', 
                stopwords = stopwords, 
                min_font_size = 10).generate(titleString) 
  
    # plot the WordCloud image                        
    plt.figure(figsize = (8, 8), facecolor = None) 
    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    
    # Save the plot in a file
    fileName = 'Wordclouds/' + subverse + '_wc.png'
    plt.savefig(fileName)
        
def voat_history_query(voatCredentials, queryWordList=None, dateBeginning=None, dateEnd=None, subverseList=None, 
                       bodySearch=False, nsfw='off', firstCall=True, web=None, submissionResult=[], linkAnalysis=True, source=None,
                       commentsAnalysis=False, commentDict=None):
    
    '''
    Method running a query on the Voat history, to retrieve any submission related to a specific topic. The other parameters can be used to refine the query
    '''
         
    pageCount = 0
    print('Function call: ', dateBeginning, dateEnd)

    if firstCall:
        web = Browser()
        submissionResult = []
        commentDict = []
        firstCall = False

    if queryWordList is not None:
        if subverseList is not None:
            query = '&'.join(['t=' + '+'.join(queryWordList), 's=' + '%2C+'.join(subverseList), 'df=' + dateBeginning, 'dt=' + dateEnd, 'nsfw=' + nsfw])
        else:
            query = '&'.join(['t=' + '+'.join(queryWordList), 'df=' + dateBeginning, 'dt=' + dateEnd, 'nsfw=' + nsfw])
            
    else:
        if subverseList is not None:
            query = '&'.join(['s=' + '%2C+'.join(subverseList), 'df=' + dateBeginning, 'dt=' + dateEnd, 'nsfw=' + nsfw])
        else:
            query = '&'.join(['df=' + dateBeginning, 'dt=' + dateEnd, 'nsfw=' + nsfw])
    
    if bodySearch:
        query += '&b=on'
        
    if source is not None:
        query += '&d=' + source
        
    urlQuery = 'searchvoat.co/?' + query
    web.go_to(urlQuery)
    
    time.sleep(5)
    voatContent = web.get_page_source()
    voatSoup = bs(voatContent, features='lxml')
    
    if any(errorText in voatSoup.find('body').text for errorText in ['200 per day', "This site can't be reached", 'Your connection was interrupted']):
        change_ip()
        print('New IP Address: ', os.popen('curl ifconfig.me').read())
        time.sleep(60)
        web.go_to(urlQuery)
        
        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')
    
    pResult = [pTag for pTag in voatSoup.find_all('p') if 'found,' in pTag.text and pTag.text.split()
               [pTag.text.split().index('found,') - 1].replace(',', '').isdigit()]
    
    if len(pResult) == 0:
        return
    else:
        numberResults = int(pResult[0].text.split(' ')[0].replace(',', ''))
 
    if numberResults > 240: # 240 is the maximum limit of query results allowed by searchvoat
        if dateBeginning != dateEnd:
            numberSegments = math.ceil(numberResults / 240)
            datetimeBeginning = datetime.strptime(dateBeginning, '%Y-%m-%d').date()
            datetimeEnd = datetime.strptime(dateEnd, '%Y-%m-%d').date()

            dayDifference = (datetimeEnd - datetimeBeginning).days
            dayDelta = dayDifference / numberSegments
            print(dayDelta)
            
            if dayDelta >= 1:
                for dayStep in range(0, numberSegments):

                    stepBeginning = datetimeBeginning + dayStep * timedelta(days=dayDelta)
                    stepEnd = datetimeBeginning + (dayStep + 1) * timedelta(days=dayDelta)
                    if dayStep > 0:
                        stepBeginning += timedelta(days=1)

                    # RECURSIVE METHOD: Call the method with the new time period
                    voat_history_query(voatCredentials, queryWordList, stepBeginning.strftime('%Y-%m-%d'), stepEnd.strftime('%Y-%m-%d'), subverseList, bodySearch, 
                                       nsfw, firstCall, web, submissionResult, linkAnalysis, source, commentsAnalysis, commentDict=commentDict)
                    
            else:
                for dayStep in range(0, dayDifference + 1):
                    dayQuery = datetimeBeginning + timedelta(days=dayStep)
                    
                    dateBeginningHour = [dayQuery.strftime('%Y-%m-%d') + ' 00:00:00', dayQuery.strftime('%Y-%m-%d') + ' 06:00:00', 
                                         dayQuery.strftime('%Y-%m-%d') + ' 12:00:00', dayQuery.strftime('%Y-%m-%d') + ' 18:00:00']
                    dateEndHour = [dayQuery.strftime('%Y-%m-%d') + ' 06:00:00', dayQuery.strftime('%Y-%m-%d') + ' 12:00:00', 
                                   dayQuery.strftime('%Y-%m-%d') + ' 18:00:00', dayQuery.strftime('%Y-%m-%d') + ' 23:59:59']
                    
                    for hourBeginning, hourEnd in zip(dateBeginningHour, dateEndHour):

                        # RECURSIVE METHOD: Call the method with the new time period
                        voat_history_query(voatCredentials, queryWordList, hourBeginning, hourEnd, subverseList, bodySearch, 
                                           nsfw, firstCall, web, submissionResult, linkAnalysis, source, commentsAnalysis, commentDict=commentDict)
                    
                
        else: # it is necessary to split the day to run the query for a specific number of hours
            dateBeginningHour = [dateBeginning + ' 00:00:00', dateBeginning + ' 06:00:00', dateBeginning + ' 12:00:00', dateBeginning + ' 18:00:00']
            dateEndHour = [dateEnd + ' 06:00:00', dateEnd + ' 12:00:00', dateEnd + ' 18:00:00', dateEnd + ' 23:59:59']
            
            for hourBeginning, hourEnd in zip(dateBeginningHour, dateEndHour):
                voat_history_query(voatCredentials, queryWordList, hourBeginning, hourEnd, subverseList, bodySearch, nsfw, firstCall, web, submissionResult, 
                                   linkAnalysis, source, commentsAnalysis, commentDict=commentDict)
                
            
    else:
        stopCondition = False
        while not stopCondition:
            
            currentURL = web.get_current_url()
            
            submissionList = voatSoup.find_all('div', {'class' : 'submission'})
            submissionPage = [{'title': submission.find('p', {'class' : 'title'}).text.replace('\n', ''), 
                                'source': submission.find('span', {'class' : 'domain'}).text.replace('(', '').replace(')', '')
                                if submission.find('span', {'class' : 'domain'}) else submission.find('a', {'class' : 'subverse'}).text,
                                'author': submission.find('a', {'class' : 'author'}).text 
                                if submission.find('a', {'class' : 'author'}) else 'Unknown',
                                'subverse': submission.find('a', {'class' : 'subverse'}).text,
                                'date': datetime.strptime(submission.find('time')['title'], '%Y-%m-%d %H:%M:%S %Z').date().strftime('%Y-%m-%d'),
                                'time': datetime.strptime(submission.find('time')['title'], '%Y-%m-%d %H:%M:%S %Z').time().strftime('%H:%M:%S'),
                                'id': submission.find('a', {'class' : 'title'})['href'].split('/')[-1]
                                } for submission in submissionList]
            
            if linkAnalysis:
                for subIndex, submission in enumerate(submissionList):
                    voatLink = submission.find('a', {'class' : 'title'})['href']
                    submissionID = submission.find('a', {'class' : 'title'})['href'].split('/')[-1]
                    submissionSubverse = submission.find('a', {'class' : 'subverse'}).text
                    web.go_to(voatLink)
                    voatContent = web.get_page_source()
                    voatSoup = bs(voatContent, features='lxml')
                    
                    if commentsAnalysis:
                        commentDict = voat_comments_retrieval(voatSoup, dataDepth=0, rootID=submissionID, subverse=submissionSubverse,
                                                                           commentDict=commentDict)

                    if 'HTTP ERROR 500' in voatSoup.text:
                        time.sleep(5)
                        web.go_to(voatLink)
                        voatContent = web.get_page_source()
                        voatSoup = bs(voatContent, features='lxml')

                    if 'welcome back fancy pants' in voatSoup.text.lower():
                        web = voat_login(voatCredentials, web)
                        voatContent = web.get_page_source()
                        voatSoup = bs(voatContent, features='lxml')

                    if submissionPage[subIndex]['subverse'] == submissionPage[subIndex]['source']:
                        submissionPage[subIndex]['link'] = submissionPage[subIndex]['subverse']
                        submissionPage[subIndex]['link_root'] = submissionPage[subIndex]['subverse']

                    else:
                        submissionLink = voatSoup.find('a', {'class' : 'title'})['href']
                        submissionPage[subIndex]['link'] = submissionLink
                        submissionPage[subIndex]['link_root'] = re.sub(r'(.*://)?([^/?]+).*', '\g<1>\g<2>', submissionLink).split('//')[-1]
                    time.sleep(2)
                
            submissionResult.extend(submissionPage)
            web.go_to(currentURL)
            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')
            
            pageCount += 1
            if len(voatSoup.find_all('a', string='next >')) > 0:
                newURL = urlQuery + '&p=' + str(pageCount)
                web.click('next >', tag='a')
                time.sleep(5)

                voatContent = web.get_page_source()
                voatSoup = bs(voatContent, features='lxml')
                
                if any(errorText in voatSoup.find('body').text 
                       for errorText in ['200 per day', "This site can't be reached", 'Your connection was interrupted']):
                    change_ip()
                    print('New IP Address: ', os.popen('curl ifconfig.me').read())
                    time.sleep(60)
                    web.go_to(newURL)
   
            else:
                stopCondition = True

            
    submissionDF = pd.DataFrame(submissionResult)
    
    if commentsAnalysis:
        commentDF = pd.DataFrame(commentDict)
        return submissionDF, commentDF
    else:
        return submissionDF

def voat_comments_retrieval(rootComment, dataDepth, rootID, subverse, commentDict=None):
    
    if rootComment.find('a', {'class' : 'author'}) is not None:
        rootAuthor = rootComment.find('a', {'class' : 'author'}).text
        
        commentList = rootComment.find_all('div', {'class' : 'comment', 'data-comment-depth': str(dataDepth)})  
        commentDict.extend([{'id': comment['data-comment-id'],
                        'author': comment.find('a', {'class' : 'author'}).text,
                        'rootID': rootID, 'subverse': subverse,
                        'upvotes': comment['data-ups'], 'downvotes': comment['data-downs'],
                        'text': comment.find('div', {'class' : 'usertext-body'}).find('p').text
                        if comment.find('div', {'class' : 'usertext-body'}).find('p') is not None else 'None',
                        'comment-depth': dataDepth} for comment in commentList if comment.find('a', {'class' : 'author'}) is not None])

        for comment in commentList:
            commentID = comment['data-comment-id']
            voat_comments_retrieval(comment, dataDepth=dataDepth+1, rootID=commentID, subverse=subverse, commentDict=commentDict)
    
    return commentDict

def voat_network_creation():
    
    '''
    By using the comments and submissions retrieved with the voat_history_query method, this method creates a directed network,
    where two users are connected to each other if they have been interacting in the comments section
    '''
    
    voatNetwork = nx.DiGraph()
    submissionsDF = pd.read_csv('Comments Analysis/submissionsDF.csv', usecols=['author', 'id', 'subverse'])
    commentsDF = pd.read_csv('Comments Analysis/commentsDF.csv')
    
    submissionsDF = submissionsDF.loc[submissionsDF.author != 'anon'].reset_index()
    for subIndex, submission in submissionsDF.iterrows():
        commentList = commentsDF.loc[commentsDF.rootID == submission.id]
        if len(commentList) > 0:
            
            if submission.author not in voatNetwork.nodes():
                voatNetwork.add_node(submission.author)
                
            for commIndex, comment in commentList.iterrows():
                if comment.author not in voatNetwork.nodes():
                    voatNetwork.add_node(comment.author)
                    
                if not voatNetwork.has_edge(comment.author, submission.author):
                    voatNetwork.add_edge(comment.author, submission.author, subverse=submission.subverse, weight=1)
                else:
                    voatNetwork[comment.author][submission.author]['weight'] += 1
    
    print(len(voatNetwork.nodes()), len(voatNetwork.edges()))
    filteredEdges = [edge for edge in voatNetwork.edges(data=True) if edge[2]['weight'] <= 1]
    voatNetwork.remove_edges_from(filteredEdges)
    
    nodesFiltered = [node for (node, degree) in voatNetwork.degree if degree == 0]
    voatNetwork.remove_nodes_from(nodesFiltered)
    print(len(voatNetwork.nodes()), len(voatNetwork.edges()))
    
    # For each user, a property is added to indicate the subverse they have mostly been active on
    for user in voatNetwork.nodes():
        userSubverses = list(submissionsDF.loc[submissionsDF.author == user].subverse.values)
        userSubverses.extend(commentsDF.loc[commentsDF.author == user].subverse.values)
        voatNetwork.nodes[user]['subverse'] = Counter(userSubverses).most_common(1)[0][0]
        
    # Compute, for each user, the number of edges going from them to a node in another subverse
    voatUndirected = voatNetwork.to_undirected()
    subverseCountList = []
    for user in voatUndirected.nodes():
        voatNetwork.nodes[user]['subverseCount'] = len([edge for edge in voatUndirected.edges(user) 
                                                         if voatUndirected.nodes[edge[0]]['subverse'] == voatUndirected.nodes[edge[1]]['subverse']])
        
        subverseCountList.append(voatNetwork.nodes[user]['subverseCount'])
        
    return voatNetwork, subverseCountList
                
def change_ip():
    
    '''
    Use the Express VPN Wrapper to change the IP address
    '''
    
    maxAttempts = 10
    attempts = 0
    
    while True:
        attempts += 1
        
        try:
            print('GETTING NEW IP')
            wrapper.random_connect()
            print('SUCCESS')
            return
        
        except Exception as e:
            if attempts > maxAttempts:
                print('Max attempts reached for VPN. Check its configuration.')
                exit(1)
            print(e)
            print('Skipping exception.')
            
def conspiracy_time_analysis(conspiracyDF, topic):
    
    '''
    Given the search results for a specific topic, this method analyzes the submission dates in order to see if there is a peak of activity at a specific time
    '''
    
    dateCounter = Counter(list(conspiracyDF.date.values))
    dateDict = [{'date': date, '# submissions': dateCounter[date]} for date in dateCounter.keys()]
    
    dateDF = pd.DataFrame(dateDict).sort_values(by='date').set_index('date')
    
    ax = dateDF.plot.line(figsize=(20, 10))
    ax.get_legend().remove()
    ax.title.set_text('# of Submissions on VOAT about ' + topic)
    ax.set_xlabel('Date')
    ax.set_ylabel('Submission Count')
    
    fig = ax.get_figure()
    fig.savefig('Time Graphs/' + topic + '.png')
    
def subverse_analysis(subverse):
    
    '''
    By looking at the activity on a specific subverse, this method studies the involvement of the different users, to see if some of them are much more 
    active than the average one
    '''
    
    subverseDF = voat_history_query(dateBeginning='2020-05-01', dateEnd='2020-05-12', subverseList=[subverse])
    
    return subverseDF

def topic_wordcloud(topic):
    
    '''
    This method uses the DataFrame generated with the voat_history_query method to generate a wordcloud, using the submissions related to a specific topic
    '''
    
    titleString = ''
    stopwords = set(STOPWORDS)
    
    topicDF = pd.read_csv('Search Queries/' + topic + '.csv')
    topicTitles = [re.sub(r'\([^)]*\)', '', title).lower() for title in list(topicDF.title.values)]
    
    topicString = ' '.join(topicTitles)
    
    wordcloud = WordCloud(width = 800, height = 800, 
                background_color ='white', 
                stopwords = stopwords, 
                min_font_size = 10).generate(topicString) 
  
    # plot the WordCloud image                        
    plt.figure(figsize = (8, 8), facecolor = None) 
    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    
    # Save the plot in a file
    fileName = 'Wordclouds/' + topic + '_wc.png'
    plt.savefig(fileName)
    
def country_subverse_extraction(voatCredentials):
    
    web = Browser()
    countryDict = []
    
    pyCountryList = [country.name for country in pycountry.countries]
    countryInfo = pd.read_csv('../Geographic Data/countryInfo.txt', sep="	", header=None, usecols=[4, 5, 9]).dropna()
    countryInfo.columns = ['country', 'capital', 'domain']

    countryList = list(set(list(countryInfo.country.values)).intersection(pyCountryList))
    for country in countryList:
        
        time.sleep(2)
        urlQuery = 'https://searchvoat.co/?st=subverses&t=' + country 
        web.go_to(urlQuery)
    
        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')
        
        if any(errorText in voatSoup.find('body').text for errorText in ['200 per day', "This site can't be reached", 'Your connection was interrupted']):
            change_ip()
            print('New IP Address: ', os.popen('curl ifconfig.me').read())
            time.sleep(60)
            web.go_to(urlQuery)
            
            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')
    
        pResult = [pTag for pTag in voatSoup.find_all('p') if 'found,' in pTag.text and pTag.text.split()
                   [pTag.text.split().index('found,') + 1].replace('.', '').isdigit()]
        
        if pResult[0].text.split(' ')[0].replace(',', '') == 'None':
            numberResults = 'None'
        else:
            web.go_to('https://voat.co/v/' + country.replace(' ', ''))
            time.sleep(5)
            
            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')
            
            if 'welcome back fancy pants' in voatSoup.text.lower():
                web = voat_login(voatCredentials, web)
                voatContent = web.get_page_source()
                voatSoup = bs(voatContent, features='lxml')
                
            if any(errorWord in voatSoup.text.lower() for errorWord in ['the subverse you were looking for has been disabled and is no longer accessible', 
                                                                        'the subverse you were looking for has been disabled and is no longer accessible',
                                                                        'whoops!']):
                continue
            
            numberSubscribers = voatSoup.find('span', {'id' : 'subscriberCount'}).text
            countryDict.append({'country': country, 'Subscribers': numberSubscribers})
            
    countryDF = pd.DataFrame(countryDict)
    return countryDF

def us_subverse_extraction(voatCredentials):
    
    '''
    This method extracts the number of subscribers per state-related subverse
    '''
    
    statesList = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado",
                  "Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois",
                  "Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland",
                  "Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana",
                  "Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York",
                  "North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania",
                  "Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah",
                  "Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]
    
    statesDict = []
    
    web = Browser()
    
    for state in statesList:
        time.sleep(2)
        urlQuery = 'https://searchvoat.co/?st=subverses&t=' + state.replace(' ', '')
        web.go_to(urlQuery)
    
        voatContent = web.get_page_source()
        voatSoup = bs(voatContent, features='lxml')
        
        if any(errorText in voatSoup.find('body').text for errorText in ['200 per day', "This site can't be reached", 'Your connection was interrupted']):
            change_ip()
            print('New IP Address: ', os.popen('curl ifconfig.me').read())
            time.sleep(60)
            web.go_to(urlQuery)
            
            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')
    
        pResult = [pTag for pTag in voatSoup.find_all('p') if 'found,' in pTag.text and pTag.text.split()
                   [pTag.text.split().index('found,') + 1].replace('.', '').isdigit()]
        
        if pResult[0].text.split(' ')[0].replace(',', '') == 'None':
            numberResults = 'None'
        else:
            web.go_to('https://voat.co/v/' + state.replace(' ', ''))
            time.sleep(5)
            
            voatContent = web.get_page_source()
            voatSoup = bs(voatContent, features='lxml')
            
            if 'welcome back fancy pants' in voatSoup.text.lower():
                web = voat_login(voatCredentials, web)
                voatContent = web.get_page_source()
                voatSoup = bs(voatContent, features='lxml')
                
            if any(errorWord in voatSoup.text.lower() for errorWord in ['subverse disabled', 'whoops!']):
                continue
            
            numberSubscribers = voatSoup.find('span', {'id' : 'subscriberCount'}).text
            statesDict.append({'state': state, 'Subscribers': numberSubscribers})
            
    statesDF = pd.DataFrame(statesDict)
    return statesDF
        

def country_subverse_plot(worldDF):
    
    '''
    Uses the DataFrame generated with the country_subverse_extraction method to plot a map displaying the number of subscribers per country
    '''
    
    countryInfo = pd.read_csv('../Geographic Data/countryInfo.txt', sep="	", header=None, usecols=[1, 4, 8]).dropna()
    countryInfo.columns = ['ISO', 'country', 'continent']
    countryInfo.loc[countryInfo.country == 'Canada', 'continent'] = 'NA'
    
    subscriptionDF = countryInfo.loc[countryInfo.country.isin(list(worldDF.country.values))]
    
    for index, row in worldDF.iterrows():
        subscriptionDF.loc[subscriptionDF.country == row.country, 'Subscribers'] = row.Subscribers

    subscriptionDF = subscriptionDF.replace(['AS', 'EU', 'SA', 'NA', 'AF', 'OC'], ['Asia', 'Europe', 'South America', 'North America', 'Africa', 'Oceania'])
    print(subscriptionDF.loc[subscriptionDF.country == 'Canada'])
    print(subscriptionDF.loc[subscriptionDF.country == 'Sweden'])
    fig = px.scatter_geo(subscriptionDF, locations="ISO", color="continent",
                     hover_name="country", size="Subscribers", hover_data={'ISO': False, 'continent': False, 'Subscribers': True},
                  )
    fig.update_layout(
        title = 'Number of subscribers per country-related subverses'
    )
    
    fig.show()
    fig.write_html("Geo Plots/worldMap.html")
    
def us_subverse_plot(statesDF):
    
    locator = Nominatim(user_agent='myGeocoder')
    
    for index, row in statesDF.iterrows():
        location = locator.geocode(row.state + ', USA')
        statesDF.loc[statesDF.state == row.state, 'latitude'] = location.latitude
        statesDF.loc[statesDF.state == row.state, 'longitude'] = location.longitude
    
    
    fig = px.scatter_geo(statesDF, lat='latitude', lon='longitude',
                     hover_name='state', size='Subscribers', hover_data={'Subscribers': True, 'longitude': False, 'latitude': False},
                     scope='usa')
    fig.update_layout(
        title = 'Number of subscribers per state-related subverses'
    )
    fig.show()
    fig.write_html("Geo Plots/usaMap.html")
    
def voat_login(voatCredentials, web):
    
    web.type(voatCredentials['username'] , into='UserName')
    web.type(voatCredentials['password'], into='Password')
    
    web.click('Log in', tag='input')
    return web

def twitter_query_analysis(submissionDF, topic, twitterAPI):
    
    twitterRows = submissionDF[submissionDF.link_root.isin(['twitter.com', 'mobile.twitter.com'])]
    twitterDict = []
    
    print(len(twitterRows))
    for index, submission in twitterRows.iterrows():
        try:
            tweet = twitterAPI.get_status(submission['link'].split('.com/')[1].split('/')[-1], tweet_mode='extended')
            tweetUser = twitterAPI.get_user(tweet.user.screen_name)
            twitterDict.append({'author': tweetUser.screen_name, 'username': tweetUser.name, 'followers': int(tweetUser.followers_count),
                                'account_date': tweetUser.created_at.strftime("%Y-%m-%d"), 'account_time': tweetUser.created_at.strftime("%H:%M:%S"),
                                'text': tweet.full_text, 'tweet_date': tweet.created_at.strftime("%Y-%m-%d"), 'tweet_time': tweet.created_at.strftime("%H:%M:%S"),
                                'id': tweet.id_str})

        except tweepy.TweepError:
            pass
    
    twitterDF = pd.DataFrame(twitterDict)
    twitterDF.to_csv('Search Queries/' + topic + '_twitter.csv')
    
    return twitterDF

def youtube_query_analysis(submissionDF, topic):
    
    '''
    Retrieves the Youtube links retrieved with the voat_history_query method in order to analyse the links and return a DataFrame with more information about each video
    '''
    
    youtubeRows = submissionDF[submissionDF.link_root.isin(['www.youtube.com', 'youtu.be', 'youtube.com'])]
    
    for index, youtubeVideo in youtubeRows.iterrows():
        
        youtubeRequest = requests.get(youtubeVideo['link'])
        youtubeSoup = bs(youtubeRequest.text, 'html.parser')
   
        try:
            youtubeVideo['youtube_title'] = youtubeSoup.find('meta', attrs={'itemprop' : 'name'})
            if youtubeVideo['youtube_title'] is not None:
                youtubeVideo['youtube_title'] = youtubeVideo['youtube_title']['content']

            youtubeVideo['channel'] = youtubeSoup.find('script', attrs={'type' : 'application/ld+json'})
            if youtubeVideo['channel'] is not None:
                youtubeVideo['channel'] = youtubeVideo['channel'].text.split('"name":')[1].split('\n')[0].strip().replace('"', '')

            if all(submissionInfo is not None for submissionInfo in [youtubeVideo['youtube_title'], youtubeVideo['channel']]):
                youtubeVideo['view_count'] = int(float(youtubeSoup.find('div', {'class' : 'watch-view-count'}).text.split(' ')[0].replace(',', '').replace('.', '')))
                #youtubeVideo['likes'] = youtubeSoup.find('button', {'title' : 'I like this'}).text
                #youtubeVideo['dislikes'] = youtubeSoup.find('button', {'title' : 'I dislike this'}).text  
                
        except (ConnectTimeout, HTTPError, ReadTimeout, Timeout, ConnectionError):
            print("Error")
            pass
        
    return youtubeRows

def voat_hierarchy_analysis(voatCredentials, subverseList):
    
    '''
    This method analyses the submissions from a specific set of subverses, to see if a core of users is mainly responsible of the activity, and other users only act as 
    followers
    '''
    
    submissionDF = voat_history_query(voatCredentials=voatCredentials, dateBeginning='2020-04-01', dateEnd='2020-05-24', 
                                      subverseList=subverseList)
    
    user_hierarchy_plot(submissionDF, subverseList)
    
    return submissionDF

def user_hierarchy_plot(submissionDF, subverseList):
    
    '''
    Given a DataFrame with a list of submissions, this method will plot a lollipop chart displaying the most active users, as well as a radar chart for each one of them,
    to show in which subverse they have mostly been active
    '''
    
    
    # PLOT 1: Lollipop plot
    
    userCount = dict(Counter(list(submissionDF.author.values)))
    
    userDF = pd.DataFrame.from_dict(userCount, orient='index', columns=['submission_count']).sort_values(by='submission_count', ascending=False).head(10)
    
    plt.hlines(y=range(1,len(userDF.index)+1), xmin=0, xmax=userDF['submission_count'], color='skyblue')
    plt.plot(userDF['submission_count'], range(1,len(userDF.index)+1), "o")
 
    # Add titles and axis names
    plt.yticks(range(1,len(userDF.index)+1), userDF.index)
    plt.title("Most active users in " + subverseList[0])
    plt.xlabel('# Submissions')
    plt.ylabel('Users')
    plt.tight_layout()
    plt.savefig('Hierarchy Analysis/topUsers_' + subverseList[0] + '.png')
    
    # PLOT 2: Radar Chart
    
    topUsers = list(userDF.head(5).index)
    subverseDF = pd.DataFrame(columns=subverseList)
    sourceDF = pd.DataFrame()
    
    for user in topUsers:
        userDF = submissionDF[submissionDF.author == user]
        
        subverseCount = Counter(list(userDF.subverse.values))
        
        for subverse, count in subverseCount.items():
            subverseDF.loc[user, subverse] = count
            
        sourceCount = Counter(list(userDF.source.values))
        
        for source, count in sourceCount.items():
            sourceDF.loc[user, source] = count
        
    subverseDF = subverseDF.fillna(0)
    sourceDF = sourceDF.fillna(0)
    
    subverseList = list(subverseDF.columns)
    colorList = plt.cm.get_cmap("Set2", len(topUsers))
    
    for userIndex, user in enumerate(topUsers):
        
        # Subverse plots
        
        userSubverseDF = subverseDF[subverseDF.index == user].replace(0, np.nan).dropna(axis=1)
        print(userSubverseDF)
        userSubverse = userSubverseDF.values.tolist()[0]
        subverseList = list(userSubverseDF.columns)
        
        numSubverse = len(subverseList)
        print(numSubverse)
        if numSubverse > 1:
            subverseAngles = np.linspace(0, 2 * np.pi, numSubverse, endpoint=False).tolist()
            userSubverse += userSubverse[:1]
            subverseAngles += subverseAngles[:1]
            print(userSubverse, subverseAngles)
            fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(polar=True))
            # Draw the outline of our data.
            ax.plot(subverseAngles, userSubverse, color=colorList(userIndex), linewidth=1)
            ax.set_rlabel_position(90)
            # Fill it in.
            ax.fill(subverseAngles, userSubverse, color=colorList(userIndex), alpha=0.25)
            ax.set_title('Subverse Distribution for ' + user, weight='bold', size='medium')
            plt.xticks(subverseAngles[:-1], subverseList, color='black', size=8)
        
        # Source plots
        
        userSourceDF = sourceDF[sourceDF.index == user].replace([0, 1, 2], np.nan).dropna(axis=1)
        userSource = userSourceDF.values.tolist()[0]
        sourceList = list(userSourceDF.columns)
        
        numSource = len(sourceList)
        sourceAngles = np.linspace(0, 2 * np.pi, numSource, endpoint=False).tolist()
        userSource += userSource[:1]
        sourceAngles += sourceAngles[:1]
        
        fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(polar=True))
        # Draw the outline of our data.
        ax.plot(sourceAngles, userSource, color='red', linewidth=1)
        ax.set_rlabel_position(90)
        # Fill it in.
        ax.fill(sourceAngles, userSource, color='red', alpha=0.25)
        ax.set_title('Source Distribution for ' + user, weight='bold', size='medium')
        plt.xticks(sourceAngles[:-1], sourceList, color='black', size=9)
        plt.tight_layout()
        plt.savefig('Hierarchy Analysis/source_' + subverseList[0] + '_' + user + '.png')
        
def subverse_intersection_analysis(voatCredentials, startDate, stopDate):
    
    '''
    Given a specific time period, this method retrieves every submission posted on any subverse and collects pairs consisting of the username and the subverse.
    The goal is to create a network, where each node is a subverse. Two subverses are connected together if they have a similar poster.
    '''
    
    subverseDict = defaultdict(list)
    subverseList = []
    submissionDF = voat_history_query(voatCredentials=voatCredentials, queryWordList=['a'], dateBeginning=startDate, dateEnd=stopDate, 
                                      bodySearch=True, nsfw='on', linkAnalysis=False)
    
    for index, row in submissionDF.iterrows():
        if row.author != 'anon':
            subverseList.append(row.subverse)
            
            if row.subverse not in subverseDict.keys():
                subverseDict[row.subverse] = [row.author]
            else:
                subverseDict[row.subverse].append(row.author)
    
    # Count the number of submissions per subverse
    subverseCount = Counter(subverseList)
    
    # Subverse network creation
    subverseGraph = nx.Graph()
    subverseGraph.add_nodes_from(list(subverseDict.keys()))
    nx.set_node_attributes(subverseGraph, name='submissionCount', values=subverseCount)
    
    for subverse1, subverse2 in combinations(list(subverseDict.keys()), 2):
        weightEdge = len(set(subverseDict[subverse1]).intersection(subverseDict[subverse2]))
        
        if weightEdge > 0:
            subverseGraph.add_edge(subverse1, subverse2, weight=weightEdge)
    
    # Node filtering
    
    nodesFiltered = [node for (node, degree) in subverseGraph.degree if degree == 0]
    subverseGraph.remove_nodes_from(nodesFiltered)
    
    # Community detection
    
    partition = community.best_partition(subverseGraph)
    nx.set_node_attributes(subverseGraph, name='community', values=partition)
    
    return subverseGraph

def twitter_posts_retrieval(voatCredentials, twitterAPI, dateBeginning, dateEnd, queryWordList=None, subverseList=None, bodySearch=False, nsfw='off'):
    
    '''
    By using the voat_history_query method, this method retrieves every submission sharing a link to Twitter and retrieves the metadata for each tweet
    '''
    
    submissionDF = voat_history_query(voatCredentials=voatCredentials, queryWordList=queryWordList, dateBeginning=dateBeginning, dateEnd=dateEnd, subverseList=subverseList, 
                       bodySearch=bodySearch, nsfw=nsfw, source='twitter.com')
    
    print("Submission: ", len(submissionDF))
    
    twitterDF = twitter_query_analysis(submissionDF=submissionDF, topic='twitter', twitterAPI=twitterAPI)
    
    return twitterDF

def seed_selection():
    
    '''
    This method retrieves every Twitter user whose tweets are shared every month on Voat and returns a DataFrame with these users
    '''
    
    voatAuthorList = []
    voatAuthorCount = []
    
    for subdir, dirs, files in os.walk('Twitter Analysis/Voat DF'):
        for file in files:
            if 'checkpoint' not in file:
                voatDF = pd.read_csv(subdir + '/' + file)
                authorCount = Counter(list(voatDF.author.values))
                voatAuthorCount.append(authorCount)
                authorList = list(voatDF.author.unique())
                voatAuthorList.append(authorList)
    
    authorUniqueList = list(set([author for monthList in voatAuthorList for author in monthList]))
    commonAuthors = [author for author in authorUniqueList if all(author in monthList for monthList in voatAuthorList)]
    
    commonAuthorsDict = [{'author': author, 'January Count': voatAuthorCount[0][author], 'February Count': voatAuthorCount[1][author], 
                          'March Count': voatAuthorCount[2][author], 'April Count': voatAuthorCount[3][author], 'May Count': voatAuthorCount[4][author],
                          'June Count': voatAuthorCount[5][author], 'Total Count': np.sum([voatAuthorCount[idx][author] for idx in range(0, 6)])} for author in commonAuthors]
    commonAuthorsDF = pd.DataFrame(commonAuthorsDict).sort_values(by='Total Count', ascending=False)
    commonAuthorsDF = commonAuthorsDF.assign(label="")
    
    return commonAuthorsDF
        
    
def twitter_network_creation(twitterAPI, seedList, graphDepth=2, weightThres=3):
    
    '''
    By taking a specific month as input, this method generates a graph by using the month's tweet authors as seeds and connect them to anyone who retweets or mentions them
    '''
    
    twitterNetwork = nx.DiGraph()
    
    # Every mention is saved in this DataFrame, along with the users' metadata
    totalMentionDF = pd.DataFrame(columns=['created_at', 'id', 'text', 'hashtags', 'urls', 'user_id', 'screen_name', 'name', 'followers_count', 'statuses_count', 
                                           'mention_screen_name', 'mention_name', 'mention_id', 'favorites_count', 'retweet_count', 'user_community']) 
    mentionFilePath = 'Mention DataFrames/mention.csv'
    
    userTotalList = seedList # List used to keep track of the user whose tweets have already been analysed
    
    qanonHashtagList = ['patriot', 'wwg1wga', 'qanon', 'thegreatawakening', 'q', 'qarmy']
    loopTime = time.time()
    for loopIndex in range(0, graphDepth + 1):
        print('Loop time: ', time.time() - loopTime)
        loopTime = time.time()
        userMentionsList = [] # This list is used to save every user that has been mentioned, to be used as seeds for the next loop
        
        for seedUser in seedList:
            try:
                userTweets = [tweet._json for tweet in twitterAPI.user_timeline(screen_name=seedUser, count=200)]

                twitterDF = pd.DataFrame([{'created_at': tweet['created_at'], 'id': tweet['id'], 'text': tweet['text'], 'hashtags': tweet['entities']['hashtags'],
                                   'urls': tweet['entities']['urls'][0]['expanded_url'] if len(tweet['entities']['urls']) > 0 else None, 
                                   'user_mentions': tweet['entities']['user_mentions'], 'user_id': tweet['user']['id'], 'screen_name': tweet['user']
                                           ['screen_name'],
                                   'name': tweet['user']['name'], 'followers_count': tweet['user']['followers_count'], 'created_at': tweet['user']['created_at'],
                                   'statuses_count': tweet['user']['statuses_count'], 'user': tweet['user'], 'favorite_count': tweet['favorite_count'],
                                   'retweet_count': tweet['retweet_count']}
                                   for tweet in userTweets])
                
                # Mentions extraction
                mentionsList = []
                
                for index, tweet in twitterDF.iterrows():
                    userMentions = tweet['user_mentions']
                    
                    for mention in userMentions:
                        if mention['id'] == tweet['user']['id']:
                            continue
                        
                        mentionsList.append({'user': tweet['screen_name'], 'mention': mention['screen_name'], 'weight': 1, 'urls': tweet['urls'], 'text': 
                                             tweet['text'], 
                                             'retweet_count': tweet['retweet_count'], 'favorite_count': tweet['favorite_count'], 'hashtags': tweet['hashtags'], 
                                             'user_followers': tweet['followers_count']})
                        totalMentionDF = totalMentionDF.append({'created_at': tweet['created_at'], 'id': tweet['id'], 'text': tweet['text'], 'hashtags': 
                                                                tweet['hashtags'],
                                               'urls': tweet['urls'], 'user_id': tweet['user_id'], 'screen_name': tweet['screen_name'], 'name': tweet['name'],
                                               'followers_count': tweet['followers_count'], 'statuses_count': tweet['statuses_count'],
                                               'mention_screen_name': mention['screen_name'], 'mention_name': mention['name'], 'mention_id': mention['id'],
                                               'favorites_count': tweet['favorite_count'], 'retweet_count': tweet['retweet_count'], 'user_community': None},
                                               ignore_index=True)
                        
                        if len(totalMentionDF) == 10000:
                            if os.path.isfile(mentionFilePath):
                                totalMentionDF.to_csv(mentionFilePath, mode='a', header=False)
                            else:
                                totalMentionDF.to_csv(mentionFilePath, header=True)
                                
                            totalMentionDF = pd.DataFrame(columns=['created_at', 'id', 'text', 'hashtags', 'urls', 'user_id', 'screen_name', 'name', 
                                                                   'followers_count', 'statuses_count', 
                                           'mention_screen_name', 'mention_name', 'mention_id', 'favorites_count', 'retweet_count', 'user_community']) 

                if len(mentionsList) > 0:
                    mentionDF = pd.DataFrame(mentionsList)
                    mentionGrouped = mentionDF.groupby(['user','mention']).agg(weight=('weight',sum))
                    mentionGrouped = mentionGrouped[mentionGrouped.weight >= weightThres]

                    userMentionsList.extend([mention.name[1] for index, mention in mentionGrouped.iterrows()])
                    # An edge is added from the seed user to the users they mention
                    for index, mention in mentionGrouped.iterrows():
                        user1, user2 = mention.name
                        if user1 not in twitterNetwork.nodes():
                            description1 = twitterAPI.get_user(user1).description
                            twitterNetwork.add_node(user1, description=description1, 
                                                    qanon=any(qanonHashtag in map(str.lower, re.findall(r"#(\w+)", description1))
                                                             for qanonHashtag in qanonHashtagList))
                        if user2 not in twitterNetwork.nodes():
                            description2 = twitterAPI.get_user(user2).description
                            twitterNetwork.add_node(user2, description=description2, 
                                                    qanon=any(qanonHashtag in map(str.lower, re.findall(r"#(\w+)", description2))
                                                             for qanonHashtag in qanonHashtagList))
                        
                        if not twitterNetwork.has_edge(user1, user2):
                            twitterNetwork.add_edge(user1, user2, weight=mention.weight)
                        else:
                            twitterNetwork[user1][user2]['weight'] += mention.weight
                                       
            except tweepy.TweepError:
                pass
        
        seedList = list(set([mention for mention in userMentionsList if mention not in userTotalList]))
        print('Seed List: ', len(seedList))
        userTotalList.extend(userMentionsList)
    
    avgDegree = np.mean([degree for (node, degree) in twitterNetwork.degree])
    print("deg: ", avgDegree)
    nodesFiltered = [node for (node, degree) in twitterNetwork.degree if degree < 2 * avgDegree]
    twitterNetwork.remove_nodes_from(nodesFiltered)
    
    # Community detection
    
    partition = community.best_partition(twitterNetwork.to_undirected())
    nx.set_node_attributes(twitterNetwork, name='community', values=partition)
    
    # Community retrieval for the mention DataFrame
    
    if os.path.isfile(mentionFilePath):
        totalMentionDF.to_csv(mentionFilePath, mode='a', header=False)
    else:
        totalMentionDF.to_csv(mentionFilePath, header=True)
        
    totalMentionDF = pd.read_csv(mentionFilePath)
    
    for node in twitterNetwork.nodes(data=True):
        totalMentionDF.loc[totalMentionDF.screen_name==node[0], 'user_community'] = node[1]['community']
        nodeDF = totalMentionDF.loc[totalMentionDF.screen_name==node[0]]
               
        hashtagList = [hashtag['text'] if row.hashtags != '[]' else []
                       for index, row in nodeDF.iterrows() for hashtag in ast.literal_eval(row.hashtags)]
        
        if not node[1]['qanon']:
            node[1]['qanon'] = any(qanonHashtag in map(str.lower, hashtagList) for qanonHashtag in qanonHashtagList)
        
    return twitterNetwork, totalMentionDF

def voat_network_analysis():
    
    voatNetwork = nx.read_gexf('Twitter Networks/voat.gexf')
    
    voatBetweennessList = betweenness_computation(voatNetwork)
    nx.set_node_attributes(voatNetwork, name='Betweenness', values=voatBetweennessList)
    nx.write_gexf(voatNetwork, 'voat_betweenness.gexf')
    
    qanonList = [user for user in voatNetwork.nodes(data=True) if user[1]['qanon']]
    
    filteredNodeList = [user[0] for user in voatNetwork.nodes(data=True) if not user[1]['qanon']]
    voatNetwork.remove_nodes_from(filteredNodeList)
        
    qAnonBetweennessList = betweenness_computation(voatNetwork) 
    return voatBetweennessList, qAnonBetweennessList

def betweenness_computation(network):
    
    betweennessList = nx.betweenness_centrality(network)
    betweennessListSorted = {user: betweenness for user, betweenness in sorted(betweennessList.items(), key=lambda item: item[1], reverse=True)}
    
    return betweennessListSorted

def collective_influence_computation(network, distance, rankingLength):
    
    '''
    This method computes the collective influence for each node on the network. The distance argument refers to the radius of the ball used in the algorithm
    '''
    
    undirectedNetwork = network.to_undirected()
    Gcc = sorted(nx.connected_components(undirectedNetwork), key=len, reverse=True)
    unconnectedNodes = [node for node in network.nodes() if node not in Gcc[0]]
    network.remove_nodes_from(unconnectedNodes)
    
    ciRanking = []
    rankingCount = 0
    while len(network.nodes) > 0 and rankingCount < rankingLength:
        rankingCount += 1
        ciValues = {}
        shortestPathsLengths = dict(nx.all_pairs_shortest_path_length(network))
        for nodeIn in network.nodes():
            ballNode = list(set([nodeOut[0] for nodeInter in network.in_edges(nodeIn) for nodeOut in network.in_edges(nodeInter[0])]))
            ciNode = (network.out_degree(nodeIn) - 1) * np.sum([(network.out_degree(node2) - 1) for node2 in ballNode])
            ciValues[nodeIn] = ciNode
        
        ciValues = {user: ciScore for user, ciScore in sorted(ciValues.items(), key=lambda item: item[1], reverse=True)}
        ciRanking.append(list(ciValues.keys())[0])
        network.remove_node(list(ciValues.keys())[0])
        
    return ciRanking

def hashtags_retrieval():
    
    for subdir, dirs, files in os.walk('Mention DataFrames'):
        for file in files:
            if 'checkpoint' not in file:
                mentionDF = pd.read_csv(subdir + '/' + file)
                hashtagVals = list(mentionDF.hashtags.values)
                
                hashtagList = [hashtag['text'] for hashtagList in hashtagVals for hashtag in ast.literal_eval(hashtagList)]
                
                hashtagCount = sorted(Counter(hashtagList).items(), key=operator.itemgetter(1), reverse=True)
    
    return [hashtag[0] for hashtag in hashtagCount[0:20]]
    
    
                             
                        
        
        
        
        
    
            
    
    
        