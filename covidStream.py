# Module for #sad project

import pandas as pd
import pandas_bokeh 
from bokeh.plotting import figure

import os
import json
from datetime import datetime, timedelta, date
from collections import Counter, defaultdict
import functools, operator
import numpy as np
import datetime as dt
import time
from itertools import combinations
from wordcloud import WordCloud, STOPWORDS

from bs4 import BeautifulSoup as bs
import requests
from requests import ReadTimeout, ConnectTimeout, HTTPError, Timeout, ConnectionError

import pycountry
import pycountry_convert as pc

import networkx as nx

import community

import praw, psaw, tweepy
import re
from urllib.request import urlopen
from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from urllib.error import HTTPError
from random import sample
import matplotlib.colors as mcolors

from tqdm import tqdm
import matplotlib.pyplot as plt
import spacy

from prawcore.exceptions import Forbidden, NotFound

def load_countryDF():
    
    '''
    Method called to load the .csv files with data regarding countries info (cities, states, etc...) to aggregate them into a DataFrame
    '''
    
    alphaSkipList = ['AQ', 'TF', 'EH', 'PN', 'SX', 'TL', 'UM', 'VA']
    
    with open('demonyms/demonyms.json') as demonymFile:
        demonym = json.load(demonymFile)

        demonymVals = [demonymVal[0] for demonymVal in list(demonym.values())]
        demonymKeys = [demonymKey for demonymKey in list(demonym.keys())]

        demonymDict =  dict(zip(demonymVals, demonymKeys))
        demonymDict['Netherlands'] = 'Dutch'
        demonymDict['Ireland'] = 'Irish'
        demonymDict['Australia'] = 'Australian'
        demonymDict['United States'] = 'American'

    pyCountryList = [country.name for country in pycountry.countries]
    countryInfo = pd.read_csv('Geographic Data/countryInfo.txt', sep="	", header=None, usecols=[4, 5, 9]).dropna()
    countryInfo.columns = ['country', 'capital', 'domain']

    countryList = list(set(list(countryInfo.country.values)).intersection(pyCountryList))

    countryDict = [{'country': country, 'alpha2': pc.country_name_to_country_alpha2(country), 'alpha3': pc.country_name_to_country_alpha3(country), 
                    'continent': pc.country_alpha2_to_continent_code(pc.country_name_to_country_alpha2(country)),
                    'demonym': demonymDict[country], 'domain': list(countryInfo[countryInfo.country == country].domain.values)[0],
                    'capital': list(countryInfo[countryInfo.country == country].capital.values)[0]}
                    for country in countryList if pc.country_name_to_country_alpha2(country) not in alphaSkipList and country in list(demonymDict.keys())]

    countryDF = pd.DataFrame(countryDict)
    countryDF = countryDF.append({'country': 'United Kingdom', 'alpha2': 'GB', 'alpha3': 'GBR', 'continent': 'EU', 
                                  'demonym': 'British', 'domain': '.uk', 'capital': 'London'}, ignore_index=True)
    countryDF = countryDF.append({'country': 'Russia', 'alpha2': 'RU', 'alpha3': 'RUS', 'continent': 'EU',
                                  'demonym': 'Russian', 'domain': '.ru', 'capital': 'Moscow'}, ignore_index=True)
    
    return countryDF

    
def collect_urls(website, websiteSub, twitterAPI, youtubeAPI):
    
    '''
    Method used to collect data from Twitter or Youtube links shared on the r/Coronavirus subreddit
    '''
    
    # Load the DataFrames related to countries, states and cities
    statesDF = pd.read_csv('Geographic Data/statesDF.csv', usecols=['state', 'country', 'continent'])
    cityDF = pd.read_csv('Geographic Data/cityDF.csv', usecols=['city', 'country', 'continent'])
    countryDF = load_countryDF()
    
    if website == 'twitter':
        websiteDF = pd.DataFrame(columns=['user', 'text', 'followers', 'tweet_time', 'tweet_date', 'account_time', 'account_date', 'country'])
    elif website == 'youtube':
        websiteDF = pd.DataFrame(columns=['video_title', 'video_description', 'video_date', 'video_time', 'video_view', 'video_likes', 'video_dislikes',
                                          'country'])
    
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
         'Referer': 'https://cssspritegenerator.com',
         'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
         'Accept-Encoding': 'none',
         'Accept-Language': 'en-US,en;q=0.8',
         'Connection': 'keep-alive'} 

    errorCount = 0
    webCount = 0
    startTime = time.time()
    for submission in websiteSub:

        # --- COUNTRY DETECTION ---

        if submission.link_flair_text not in ['USA', 'Canada']:

            if submission.link_flair_text == 'Europe':

                regionCapital = list(countryDF[countryDF.continent == 'EU'].capital.values)
                regionDemonym = list(countryDF[countryDF.continent == 'EU'].demonym.values)
                regionStates = list(statesDF[statesDF.continent == 'EU'].state.values)
                regionCity = list(cityDF[cityDF.continent == 'EU'].city.values)
                regionCountry = list(countryDF[countryDF.continent == 'EU'].country.values)

                regionCountryDF = countryDF[countryDF.continent == 'EU']
                regionCityDF = cityDF[cityDF.continent == 'EU']
                regionStatesDF = statesDF[statesDF.continent == 'EU']

            elif submission.link_flair_text in ['Middle East', 'Central & East Asia', 'South & SE Asia']:

                regionCapital = list(countryDF[countryDF.continent == 'AS'].capital.values)
                regionDemonym = list(countryDF[countryDF.continent == 'AS'].demonym.values)
                regionStates = list(statesDF[statesDF.continent == 'AS'].state.values)
                regionCity = list(cityDF[cityDF.continent == 'AS'].city.values)
                regionCountry = list(countryDF[countryDF.continent == 'AS'].country.values)

                regionCountryDF = countryDF[countryDF.continent == 'AS']
                regionCityDF = cityDF[cityDF.continent == 'AS']
                regionStatesDF = statesDF[statesDF.continent == 'AS']

            elif submission.link_flair_text == 'Africa':

                regionCapital = list(countryDF[countryDF.continent == 'AF'].capital.values)
                regionDemonym = list(countryDF[countryDF.continent == 'AF'].demonym.values)
                regionStates = list(statesDF[statesDF.continent == 'AF'].state.values)
                regionCity = list(cityDF[cityDF.continent == 'AF'].city.values)
                regionCountry = list(countryDF[countryDF.continent == 'AF'].country.values)

                regionCountryDF = countryDF[countryDF.continent == 'AF']
                regionCityDF = cityDF[cityDF.continent == 'AF']
                regionStatesDF = statesDF[statesDF.continent == 'AF']

            elif submission.link_flair_text == 'Latin America':

                regionCapital = list(countryDF[countryDF.continent.isin(['SA', 'NA'])].capital.values)
                regionDemonym = list(countryDF[countryDF.continent.isin(['SA', 'NA'])].demonym.values)
                regionStates = list(statesDF[statesDF.continent.isin(['SA', 'NA'])].state.values)
                regionCity = list(cityDF[cityDF.continent.isin(['SA', 'NA'])].city.values)
                regionCountry = list(countryDF[countryDF.continent.isin(['SA', 'NA'])].country.values)

                regionCountryDF = countryDF[countryDF.continent.isin(['SA', 'NA'])]
                regionCityDF = cityDF[cityDF.continent.isin(['SA', 'NA'])]
                regionStatesDF = statesDF[statesDF.continent.isin(['SA', 'NA'])]

            elif submission.link_flair_text == 'Oceania':

                regionCapital = list(countryDF[countryDF.continent == 'OC'].capital.values)
                regionDemonym = list(countryDF[countryDF.continent == 'OC'].demonym.values)
                regionStates = list(statesDF[statesDF.continent == 'OC'].state.values)
                regionCity = list(cityDF[cityDF.continent == 'OC'].city.values)
                regionCountry = list(countryDF[countryDF.continent == 'OC'].country.values)

                regionCountryDF = countryDF[countryDF.continent == 'OC']
                regionCityDF = cityDF[cityDF.continent == 'OC']
                regionStatesDF = statesDF[statesDF.continent == 'OC']

            else: # The flair does not relate to any country or continent
                
                regionCapital = list(countryDF.capital.values)
                regionDemonym = list(countryDF.demonym.values)
                regionStates = list(statesDF.state.values)
                regionCity = list(cityDF.city.values)
                regionCountry = list(countryDF.country.values)

                regionCountryDF = countryDF.copy()
                regionCityDF = cityDF.copy()
                regionStatesDF = statesDF.copy()

            # GOAL: Try to see if any country name / capital city or denomym is mentioned in the submission title

            demonymDetected = [demonym for demonym in regionDemonym if demonym.lower() in re.sub(r'[^\w\s]',' ',submission.title.lower()).split(' ')]
            capitalDetected = [capital for capital in regionCapital if capital in re.sub(r'[^\w\s]',' ',submission.title).split(' ')]
            stateDetected = [state for state in regionStates if state in re.sub(r'[^\w\s]',' ',submission.title).split(' ')]
            cityDetected = [city for city in regionCity if city in re.sub(r'[^\w\s]',' ',submission.title).split(' ')]

            countriesDetected = [country for country in regionCountry 
                             if all(countryPart.lower() in re.sub(r'[^\w\s]',' ',submission.title.replace('UK', 'United Kingdom').lower()).split(' ') 
                             for countryPart in country.split(' '))]

            if len(demonymDetected) > 0:
                countriesDetected.extend([list(regionCountryDF[regionCountryDF.demonym == demonym].country.values)[0] for demonym in demonymDetected])

            if len(capitalDetected) > 0:
                countriesDetected.extend([list(regionCountryDF[regionCountryDF.capital == capital].country.values)[0] for capital in capitalDetected])

            if len(stateDetected) > 0:
                countriesDetected.extend([list(regionStatesDF[regionStatesDF.state == state].country.values)[0] for state in stateDetected])

            if len(cityDetected) > 0:
                countriesDetected.extend([list(regionCityDF[regionCityDF.city == city].country.values)[0] for city in cityDetected])

            countriesDetected = list(set(countriesDetected))

        elif submission.link_flair_text == 'USA':
            countriesDetected = ['United States']

            regionDF = countryDF.copy()

        elif submission.link_flair_text == 'Canada':
            countriesDetected = ['Canada']

            regionDF = countryDF.copy()

        if len(countriesDetected) == 0:
            countriesDetected = ['None']

        # --- TWITTER SCRAPING ---
        
        if website == 'twitter':
            urlID = submission.url.split('.com/')[1].split('/')[-1]
            urlUsername = submission.url.split('.com/')[1].split('/')[0]

            try:
                tweet = twitterAPI.get_status(urlID, tweet_mode='extended')
                tweetUser = twitterAPI.get_user(tweet.user.screen_name)
                tweetAuthor = tweetUser.screen_name
                tweetUsername = tweetUser.name
                tweetFollowers = tweetUser.followers_count
                tweetAccountAge = tweetUser.created_at
                try:
                    accountDate = tweetAccountAge.strftime("%Y-%m-%d")
                    accountTime = tweetAccountAge.strftime("%H:%M:%S")
                except ValueError:
                    accountDate = 'Unknown'
                    accountTime = 'Unknown'
                    pass

                tweetText = tweet.full_text
                tweetDateTime = tweet.created_at

                try:
                    tweetDate = tweetDateTime.strftime("%Y-%m-%d")
                    tweetTime = tweetDateTime.strftime("%H:%M:%S")
                except ValueError:
                    tweetDate = 'Unknown'
                    tweetTime = 'Unknown'
                    pass

            except tweepy.TweepError:
                errorCount += 1
                pass

            for country in countriesDetected:
                websiteDF = websiteDF.append({'user': tweetAuthor, 'text': tweetText, 'followers': tweetFollowers, 
                                              'tweet_time': tweetTime, 'tweet_date': tweetDate, 
                                              'account_time': accountTime, 'account_date': accountDate, 'country': country}, ignore_index=True)
                
        # --- YOUTUBE SCRAPING ---
        
        if website == 'youtube':
            
            try:
                youtubeSearch = youtubeAPI.search(submission.url)
                if len(youtubeSearch) > 0:
                    youtubeVideo = youtubeAPI.get_video_metadata(youtubeSearch[0]['video_id'])
                    
                    for country in countriesDetected:
                        websiteDF = websiteDF.append(
                        {'video_title': youtubeSearch[0]['video_title'], 'video_description': youtubeSearch[0]['video_description'],
                         'video_date': youtubeSearch[0]['video_publish_date'].strftime("%Y-%m-%d"), 'video_time': youtubeSearch[0]['video_publish_date'].strftime("%H:%M:%S"),
                         'video_view': int(youtubeVideo['video_view_count']), 'video_likes': youtubeVideo['video_like_count'],
                         'video_dislikes': youtubeVideo['video_dislike_count'], 'country': country}, ignore_index=True)

            except HTTPError:
                pass
        
    return websiteDF

def create_social_network(userList, psawAPI, submissionsAnalysis, commentsAnalysis):
    
    '''
    Method used to create a social network from a specific list of user, by linking subreddits (nodes) together if they have been visited by a same
    user. The weight is computed as the inverse of the time difference between the visit of the two subreddits
    '''
    
    userNetwork = nx.DiGraph()
    userNetwork.add_node('Coronavirus', submission_score=0, comment_score=0, website='None')
    totalComment, subDict = {}, {}
        
    userWebsites = [user['website'] for user in userList]
    
    websiteList = ['foxnews' if 'foxnews' in website
                else 'liveleak' if 'liveleak' in website
                else 'breitbart' if 'breitbart' in website
                else 'infowars' if 'infowars' in website
                else 'shtfplan' if 'shtfplan' in website
                else 'lewrockwell' if 'lewrockwell' in website
                else 'zerohedge' if 'zerohedge' in website
                else 'mercola' if 'mercola' in website
                else 'unknown'
                for website in userWebsites]
    
    if submissionsAnalysis:
        for userIndex, user in enumerate(userList):
            userActivity = psawAPI.redditor_subreddit_activity(user['user'])
            userSubmissions = list(psawAPI.search_submissions(author=user['user']))
            
            for submission in userSubmissions:
                if submission.subreddit.display_name not in userNetwork.nodes():
                    userNetwork.add_node(submission.subreddit.display_name, submission_score=1,
                                             website=websiteList[userIndex])
                else:
                    userNetwork.nodes[submission.subreddit.display_name]['submission_score'] += 1
                    
            for submission1, submission2 in combinations(userSubmissions, 2):
                if submission1.subreddit.display_name != submission2.subreddit.display_name:

                    if submission1.created_utc > submission2.created_utc: # the first submission has been posted later than the first one
                        edgeWeight = 1 / (submission1.created_utc - submission2.created_utc)
                        if not userNetwork.has_edge(submission2.subreddit.display_name, submission1.subreddit.display_name):
                            userNetwork.add_edge(submission2.subreddit.display_name, submission1.subreddit.display_name, weight = edgeWeight)
                        else:
                            userNetwork[submission2.subreddit.display_name][submission1.subreddit.display_name]['weight'] += edgeWeight
                            
                    elif submission1.created_utc < submission2.created_utc: # the first submission has been posted earlier than the first one
                        edgeWeight = 1 / (submission2.created_utc - submission1.created_utc)
                        if not userNetwork.has_edge(submission1.subreddit.display_name, submission2.subreddit.display_name):
                            userNetwork.add_edge(submission1.subreddit.display_name, submission2.subreddit.display_name, weight = edgeWeight)
                        else:
                            userNetwork[submission1.subreddit.display_name][submission2.subreddit.display_name]['weight'] += edgeWeight
                            
                    else: # the two submissions have been posted at the exact same time
                        print("Exact: ", submission1.title, submission1.author.name, submission1.subreddit.display_name)
                        print("Exact2: ", submission2.title, submission2.author.name, submission2.subreddit.display_name)
                        if not userNetwork.has_edge(submission1.subreddit.display_name, submission2.subreddit.display_name):
                            userNetwork.add_edge(submission1.subreddit.display_name, submission2.subreddit.display_name, weight = 1)
                        else:
                            userNetwork[submission1.subreddit.display_name][submission2.subreddit.display_name]['weight'] += 1
    
    # Removing the nodes with a degree < filterDegree
    
    outEdgesWeights = [outEdge[2]['weight'] for outEdge in userNetwork.out_edges(data=True)]
    inEdgesWeights = [inEdge[2]['weight'] for inEdge in userNetwork.in_edges(data=True)]
    
    avgOutWeight = np.mean(outEdgesWeights)
    avgInWeight = np.mean(inEdgesWeights)
    
    filteredOutEdges = [outEdge for outEdge in userNetwork.out_edges(data=True) if outEdge[2]['weight'] < avgOutWeight / 2]
    filteredInEdges = [inEdge for inEdge in userNetwork.in_edges(data=True) if inEdge[2]['weight'] < avgInWeight / 2]
    
    userNetwork.remove_edges_from(filteredOutEdges)
    userNetwork.remove_edges_from(filteredInEdges)
    
    filteredEdges = []
    for node1, node2 in userNetwork.edges():
        if userNetwork.has_edge(node1, node2) and userNetwork.has_edge(node2, node1):
            if userNetwork[node1][node2]['weight'] > userNetwork[node2][node1]['weight']:
                filteredEdges.append([node2, node1])
            else:
                filteredEdges.append([node1, node2]) 
        
    userNetwork.remove_edges_from(filteredEdges)
    
    nodesFiltered = [node for (node, degree) in userNetwork.degree if degree == 0]
    userNetwork.remove_nodes_from(nodesFiltered)
                
    return userNetwork

def user_activity_wordcloud(username, psawAPI, subreddits=None, commentsAnalysis=False, submissionsAnalysis=False):
    
    '''
    Method used to extract the activity of a specific user on Reddit. Can be focused on specific communities if the subreddits argument is given
    '''
    
    if subreddits is not None:
        if submissionsAnalysis:
            userSubmissions = list(psawAPI.search_submissions(author=username,
                                                              subreddit=subreddits,
                                                              filter=['url','author', 'title', 'created_utc']))
        if commentsAnalysis:
            userComments = list(psawAPI.search_comments(author=username,
                                                        subreddit=subreddits,
                                                        filter=['url','author', 'title', 'created_utc']))
    else:
        if submissionsAnalysis:
            userSubmissions = list(psawAPI.search_submissions(author=username,
                                                              filter=['url','author', 'title', 'created_utc']))
        
        if commentsAnalysis:
            userComments = list(psawAPI.search_comments(author=username,
                                                        filter=['url','author', 'title', 'created_utc']))
        
    stopwords = set(STOPWORDS)
    textMerged = ""
    
    if submissionsAnalysis:
        for submission in userSubmissions:
            tokens = [token.lower() for token in submission.title.split()]
            for token in tokens:
                textMerged = textMerged + token + " "
                
    if commentsAnalysis:
        for comment in userComments:
            tokens = [token.lower() for token in comment.body.split()]
            for token in tokens:
                textMerged = textMerged + token + " "

    textMerged = re.sub('[^a-zA-Z0-9 \n]', ' ', textMerged)
    
    wordcloud = WordCloud(width = 800, height = 800, 
                background_color ='white', 
                stopwords = stopwords, 
                min_font_size = 10).generate(textMerged) 
  
    # plot the WordCloud image                        
    plt.figure(figsize = (8, 8), facecolor = None) 
    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    
    # Save the plot in a file
    fileName = username + '_wc.png'
    plt.savefig(fileName)
    
    
def user_activity_timeline(username, psawAPI, startEpoch=None, endEpoch=None):
    
    '''
    Method used to study the activity of a user over time for specific subreddits
    '''
    
    if startEpoch is None and endEpoch is None:
        userSubmissions = list(psawAPI.search_submissions(author=username,
                                                          filter=['url','author', 'title', 'created_utc']))

        userComments = list(psawAPI.search_comments(author=username,
                                                    filter=['url','author', 'title', 'created_utc']))
        
    else:
        userSubmissions = list(psawAPI.search_submissions(after=startEpoch,
                                                          before=endEpoch,
                                                          author=username,
                                                          filter=['url','author', 'title', 'created_utc']))

        userComments = list(psawAPI.search_comments(after=startEpoch,
                                                    before=endEpoch,
                                                    author=username,
                                                    filter=['url','author', 'title', 'created_utc']))
    
    createdSubs = [datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y') for submission in userSubmissions]
    createdComms = [datetime.strptime(time.ctime(comment.created_utc), '%a %b %d %H:%M:%S %Y') for comment in userComments]
    createdSubs.extend(createdComms)
    createdSubs = list(set(createdSubs))
    yearsVals = list(set([createdDate.year for createdDate in createdSubs]))
    yearsVals.sort()
    
    dfColumns = []
    for year in yearsVals:
        subsDate = [date.date() for date in createdSubs if date.year == year]
        subsDate = list(set(subsDate))
        subsDate.sort()
        dfColumns.extend([date.strftime('%d-%m-%y') for date in subsDate])
    
    userDF = pd.DataFrame(columns=dfColumns)
    
    for submission in userSubmissions:
        
        if submission.subreddit.display_name not in list(userDF.index.values):
            userDF.loc[submission.subreddit.display_name] = list(np.zeros(len(dfColumns)))
            
        subDay = datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').strftime('%d-%m-%y')
        userDF.loc[submission.subreddit.display_name][subDay] += 1
        
    userRows = [row for index, row in userDF.iterrows() if np.sum(list(row.values)) > 10]
    
    userDFFiltered = pd.DataFrame(userRows)
        
    # Line Plot
    userDFFiltered = userDFFiltered.transpose()
    
    ax = userDFFiltered.plot.line(subplots=True, figsize=(20, 10))
    fig = ax[0].get_figure()
    fig.savefig(username + '.png')
    
    userDF.to_csv(username + '_DF.csv')
    

def retrieve_user_subreddits(username, psawAPI):
    
    '''
    Method returning a list containing each subreddits where the user has posted a comment or a submission
    '''
    
    userActivity = psawAPI.redditor_subreddit_activity(username)
    userSubreddits = list(set([subreddit for subreddiList in [list(subredditDict.keys()) for subredditDict in userActivity.values()] for subreddit in subreddiList]))
    
    return userSubreddits
        

def website_retrieval(username, psawAPI):
    
    '''
    Method used to retrieve every URL shared on the subreddits in subredditList, in order to study their frequency
    '''
    
    startEpoch = int(dt.datetime(2018, 3, 24).timestamp())
    endEpoch = int(dt.datetime(2018, 3, 31).timestamp())
    
    urlCounter = Counter()
    userSubreddits = retrieve_user_subreddits(username, psawAPI)
    
    for subreddit in userSubreddits:
        subredditSubmissions = list(psawAPI.search_submissions(after=startEpoch,
                                                               before=endEpoch,
                                                               subreddit=subreddit,
                                                               filter=['url', 'title', 'created_utc']))
        
        subredditURL = [re.sub(r'(.*://)?([^/?]+).*', '\g<1>\g<2>', submission.url).split('//')[-1] for submission in subredditSubmissions]
        urlCounter += Counter(subredditURL)
    
    return urlCounter


def twitter_accounts_retrieval(subreddit, psawAPI, twitterAPI):
    
    '''
    Method used to retrieve the accounts linked by the bot on the r/thetwitterfeed subreddit, by analysing the submission history and recovering any new account
    '''
    
    tweetFeedDF = pd.DataFrame(columns=['author', 'username', 'followers', 'account_date', 'account_time'])
    
    startEpoch = int(dt.datetime(2016, 1, 1).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 5).timestamp())
    tweetList = []
    
    tweetSubmissionList = list(psawAPI.search_submissions(after=startEpoch,
                                                         before=endEpoch,
                                                         subreddit=subreddit,
                                                         filter=['url', 'title']))
    
    if subreddit == 'TheTwitterFeed':
        tweetCounter = Counter([submission.title.split(':')[0] for submission in tweetSubmissionList])
        tweetAccountsList = list(set([submission.title.split(':')[0] for submission in tweetSubmissionList]))

        for tweetAccount in tweetAccountsList:
            try:
                tweetUser = twitterAPI.get_user(tweetAccount)
                tweetAuthor = tweetUser.screen_name
                tweetUsername = tweetUser.name
                tweetFollowers = tweetUser.followers_count
                tweetAccountAge = tweetUser.created_at

                try:
                    accountDate = tweetAccountAge.strftime("%Y-%m-%d")
                    accountTime = tweetAccountAge.strftime("%H:%M:%S")

                except ValueError:
                    accountDate = 'Unknown'
                    accountTime = 'Unknown'
                    pass

                tweetList.append({'author': tweetAuthor, 'username': tweetUsername, 'followers': tweetFollowers, 'account_date': accountDate, 'account_time': accountTime,
                                  'tweet_count': tweetCounter[tweetAccount]})

            except tweepy.error.TweepError:
                print("Error with: ", tweetAccount)
                pass
            
    elif subreddit == 'ChristiansAwake2NWO':
        tweetAccountList = [submission.url.split('com/')[1].split('/')[0] for submission in tweetSubmissionList 
                            if ('twitter.com' in submission.url and len(submission.url.split('com/')) > 1)]
        tweetCounter = Counter(tweetAccountList)
        
        zetteList = []
        
        for tweetAccount in list(set(tweetAccountList)):
            try:
                tweetUser = twitterAPI.get_user(tweetAccount)
                tweetAuthor = tweetUser.screen_name
                tweetUsername = tweetUser.name
                tweetFollowers = tweetUser.followers_count
                tweetAccountAge = tweetUser.created_at

                try:
                    accountDate = tweetAccountAge.strftime("%Y-%m-%d")
                    accountTime = tweetAccountAge.strftime("%H:%M:%S")

                except ValueError:
                    accountDate = 'Unknown'
                    accountTime = 'Unknown'
                    pass

                tweetList.append({'author': tweetAuthor, 'username': tweetUsername, 'followers': tweetFollowers, 'account_date': accountDate, 'account_time': accountTime,
                                  'tweet_count': tweetCounter[tweetAccount]})
                  
            except tweepy.error.TweepError:
                print("Error with: ", tweetAccount)
                pass

    tweetFeedDF = pd.DataFrame(tweetList).sort_values(by='tweet_count', ascending=False)
    return tweetFeedDF

def youtube_accounts_retrieval(psawAPI, youtubeAPI):
    
    startTime = time.time()
    youtubeFeedDF = pd.DataFrame()
    
    startEpoch = int(dt.datetime(2020, 3, 30).timestamp())
    endEpoch = int(dt.datetime(2020, 3, 31).timestamp())
    youtubeList = []
    
    youtubeSubmissionList = list(psawAPI.search_submissions(after=startEpoch,
                                                            before=endEpoch,
                                                            subreddit='TheVideoFeed',
                                                            filter=['url', 'title']))
    
    youtubeLinks = [submission.url for submission in youtubeSubmissionList]
    youtubeChannelsList = [youtubeAPI.search(youtubeLink)[0]['channel_title'] for youtubeLink in youtubeLinks]
    print(youtubeChannelsList)
        
def subreddit_user_activity(username, subredditList, psawAPI, nrows, ncols, linkAnalysis=False, textAnalysis=False):
    
    '''
    Method used to study the behaviour of a Reddit user on specific subreddits, by gathering their comments and submissions and create a separate wordcloud
    for each subreddit. The end goal is to see if a user tends to propagate a unique mindset in every community they interact with, or if they tend to 
    change their discourse depending on the social context.
    The parameters nrows and ncols are used to specify the dimensions of the Wordcloud subplots
    '''
    
    urlDF = pd.DataFrame(columns=['url', 'subreddit'])
    
    for index, subreddit in enumerate(subredditList):
        
        print(subreddit)
        subredditSubmissionList = list(psawAPI.search_submissions(author=username,
                                                                  subreddit=subreddit,
                                                                  filter=['url', 'title']))
        
        subredditCommentList = list(psawAPI.search_comments(author=username,
                                                            subreddit=subreddit,
                                                            filter=['body']))
        
        
        # LINK ANALYSIS
        if linkAnalysis:
            urlUser = [{'subreddit': subreddit, 'url': re.search("(?P<url>https?://[^\s]+)", submission.title).group("url")} for submission in subredditSubmissionList
                              if re.search("(?P<url>https?://[^\s]+)", submission.title) is not None]
            urlUser.extend([{'subreddit': subreddit, 'url': re.search("(?P<url>https?://[^\s]+)", comment.body).group("url")} for comment in subredditCommentList
                           if re.search("(?P<url>https?://[^\s]+)", comment.body) is not None])
            
            if len(urlUser) > 0:
                urlDF = urlDF.append(urlUser)
        
        
        # TEXT ANALYSIS
        if textAnalysis:
            stopwords = set(STOPWORDS)
            textSubmission = ' '.join([token.lower() for submission in subredditSubmissionList for token in re.sub(r'[^\w\s]','', submission.title).split()])
            textComment = ''.join([token.lower() for comment in subredditCommentList 
                                                  for token in ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ", comment.body).split())])

            subredditText = textSubmission + textComment
            wordcloud = WordCloud(width = 800, height = 800, 
                        background_color ='white', 
                        stopwords = stopwords, 
                        min_font_size = 10).generate(subredditText) 

            # Plot the WordCloud image 

            plt.figure(figsize = (12, 12), facecolor = None) 
            plt.subplot(nrows, ncols, index+1)
            plt.imshow(wordcloud) 
            plt.axis("off") 
            plt.title(subreddit)
            plt.tight_layout(pad = 0)
             
    return urlDF

def newsfeed_analysis(subreddit, psawAPI):
    
    '''
    Method used to collect the submissions from a specific subreddit since the beginning of 2020, in order to see the sources followed by the users. 
    Each distinct source will be aggregated in a separate DataFrame, in which the articles titles will also be mentioned 
    '''
    
    startEpoch = int(dt.datetime(2018, 6, 1).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 3).timestamp())
    
    newsfeedSubmissionList = list(psawAPI.search_submissions(after=startEpoch,
                                                             before=endEpoch,
                                                             subreddit=subreddit,
                                                             filter=['url', 'title']))
    
    newsList = [{'website': re.sub(r'(.*://)?([^/?]+).*', '\g<1>\g<2>', submission.url).split('//')[-1], 'title': submission.title, 'link': submission.url,
                 'date': datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').date().strftime('%d-%m-%y'),
                 'time': datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').time().strftime('%H:%M:%S')}
                 for submission in newsfeedSubmissionList]
    
    newsDF = pd.DataFrame(newsList)
    
    for group in newsDF.groupby(['website']):
        groupFile = subreddit + '/' + str(group[0]) + '.csv'
        group[1].to_csv(groupFile)

def twitter_feed_user_activity(usernameList, psawAPI):
    
    '''
    Method used to plot the activity of each Twitter user followed by the Twitter feed bot (~ 20 users) to see if they have been particularly active at certain points in time during the
    Coronavirus pandemic crisis
    '''
    
    tweetFolder = 'TwitterFeed/'
    pd.set_option('plotting.backend', 'pandas_bokeh') # plot setting for pandas-bokeh
    
    startEpoch = int(dt.datetime(2020, 2, 15).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 4).timestamp())
    userDF = pd.DataFrame()
    
    for username in usernameList:
        print(username)
        userSubmissions = list(psawAPI.search_submissions(before=endEpoch,
                                                          after=startEpoch,
                                                          q=username,
                                                          filter=['created_utc']))
        
        if len(userSubmissions) > 0:
            createdDates = [datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').date() for submission in userSubmissions]
            datesStringList = [date.strftime('%m-%d') for date in createdDates]

            datesColumns = list(set(datesStringList))
            datesColumns.sort()
            datesCount = Counter(datesStringList)
            datesDict = [{'Date': date, username: datesCount[date]} for date in datesColumns]
            datesDF = pd.DataFrame(datesDict).sort_values(by='Date').set_index('Date')
            userDF = pd.concat([userDF, datesDF], axis=1)
             
    userDF = userDF.fillna(0)
    
    ax = plt.figure()
    htmlPlot = userDF.plot_bokeh(kind="line", return_html=True, xlabel='Date', ylabel='# Tweets', figsize=(1600, 900)) 
    ax.title.text = topic
    ax.legend.location = 'top_left'
    
    with open("test.html" , "w") as f:
        f.write(htmlPlot)
              
    return userDF

def news_feed_summary(folder):
    
    '''
    Creates a summary of the data retrived from the r/TheNewsFeed subreddit, by counting the number of references made to each source
    '''
    
    newsDict = []
    for fileName in os.listdir(folder):
        if fileName[-3:] == 'csv':
            sourceDF = pd.read_csv(folder + fileName)
            newsDict.append({'source': fileName[:-4], 'count': len(sourceDF)})

    newsDF = pd.DataFrame(newsDict).sort_values(by='count', ascending=False).head(20)
    #newsDF = newsDF[newsDF['count'] > 100].reset_index(drop=True)
    
    ax = newsDF.plot(kind='barh', figsize=(20, 10), legend=False, color='coral')
    ax.set_yticklabels(newsDF.source)
    ax.set_xlabel("Mentions count")
    ax.set_title('Websites mentions on r/' + folder[:-1])
    fig = ax.get_figure()
    fig.savefig(folder + folder[:-1] + 'Hist.png')
    return newsDF

def conspiratorial_topics_analysis(psawAPI, topicsList, subredditActivityList):
    
    '''
    Method used to look into the Reddit history and find any submission mentioning the coronavirus along with another controversial topic.
    The goal is to study the interaction between these two topics, and see if any community is involved in a lot of dubious information propaganda
    '''
    
    startEpoch = int(dt.datetime(2020, 3, 1).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 8).timestamp())
    subredditList, authorList = [], []
    topicDict = {}
    
    for topic in topicsList:
        
        querySubmissions = list(psawAPI.search_submissions(before=endEpoch,
                                                           after=startEpoch,
                                                           title=topic,
                                                           filter=['url', 'title', 'subreddit', 'author', 'created_utc']))
        
        querySubreddits = [submission.subreddit.display_name for submission in querySubmissions]
        topicDict[topic] = dict(Counter(querySubreddits))
        subredditList.extend(querySubreddits)
        
        topic_timeline_analysis(topic, querySubmissions)
        subreddit_timeline_analysis(subredditActivityList, topic, querySubmissions)
    
    subredditList = list(set(subredditList))
    topicDF = pd.DataFrame(columns=subredditList)
    
    for topicIndex, topic in enumerate(topicsList):
        topicDF = topicDF.append(pd.Series(data=topicDict[topic], name=topic))
        
    topicDF = topicDF.fillna(0)
    topicDF.loc['totalCount', :]= topicDF.sum(axis=0)
    
    # Network creation
    
    commonRedditorTotal = []
    
    subredditType = {subreddit : 'Coronavirus' if any(covidWord in subreddit.lower() for covidWord in ['coronavirus', 'covid'])
                                               else 'China' if any(chinaWord in subreddit.lower() for chinaWord in ['china', 'wuhan'])
                                               else 'Trump' if any(trumpWord in subreddit.lower() for trumpWord in ['donald', 'trump'])
                                               else 'None' for subreddit in subredditList}
    
    conspiracyGraph = nx.Graph()
    conspiracyGraph.add_nodes_from(list(topicDF.columns))
    nx.set_node_attributes(G=conspiracyGraph, name='subs5g', values=dict(topicDF.loc['5g']))
    nx.set_node_attributes(G=conspiracyGraph, name='subsChloroquine', values=dict(topicDF.loc['chloroquine']))
    nx.set_node_attributes(G=conspiracyGraph, name='totalCount', values=dict(topicDF.loc['totalCount']))
    nx.set_node_attributes(G=conspiracyGraph, name='subredditType', values=subredditType)
    
    # Edge computation
    
    for subreddit1, subreddit2 in combinations(list(topicDF.columns), 2):
        if not conspiracyGraph.has_edge(subreddit1, subreddit2):
            redditors1 = [submission.author for submission in querySubmissions if submission.subreddit.display_name == subreddit1 and submission.author is not None]
            redditors2 = [submission.author for submission in querySubmissions if submission.subreddit.display_name == subreddit2 and submission.author is not None]
            commonRedditors = list(set(redditors1).intersection(redditors2))

            if len(commonRedditors) > 0:
                conspiracyGraph.add_edge(subreddit1, subreddit2, weight=len(commonRedditors))
                commonRedditorTotal.extend(commonRedditors)
                
    # Remove isolated nodes
    
    nodesFiltered = [node for (node, degree) in conspiracyGraph.degree if degree == 0]
    conspiracyGraph.remove_nodes_from(nodesFiltered)
    
    # Community detection
    
    partition = community.best_partition(conspiracyGraph)
    nx.set_node_attributes(conspiracyGraph, name='community', values=partition)
    
    # Save graph
    
    nx.write_gexf(conspiracyGraph, 'conspi.gexf')
    
    return topicDF, conspiracyGraph, commonRedditorTotal

def topic_timeline_analysis(topic, querySubmissions):
    
    '''
    By using the query results obtained with the conspiratorial_topics_analysis method, we can study the trending of this topic over time in the
    Reddit community, in order to see if a peak can be observed at a specific time
    '''
    
    print("Timeline analysis")
    createdDates = [datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').date() for submission in querySubmissions]
    datesStringList = [date.strftime('%m-%d') for date in createdDates]

    datesColumns = list(set(datesStringList))
    datesColumns.sort()

    datesCount = Counter(datesStringList)
    datesDict = [{'Date': date, '# Comments': datesCount[date]} for date in datesColumns]
    datesDF = pd.DataFrame(datesDict).sort_values(by='Date').set_index('Date')
    
    ax = datesDF.plot.line(figsize=(20, 10))
    ax.get_legend().remove()
    ax.title.set_text('# of Submissions on Reddit about ' + topic)
    ax.set_xlabel('Date')
    ax.set_ylabel('Submission Count')
    
    fig = ax.get_figure()
    fig.savefig('Conspiracy Topics Analysis/' + topic + '.png')
    
def subreddit_timeline_analysis(subredditList, topic, querySubmissions):
    
    '''
    Analysis of the activity on specific subreddits (such as the top 20 inferred by the redditor_conspiracy_analysis method) to see how the controversial topics
    are mentioned over time
    '''
    
    print("Subreddit analysis")
    
    subredditDF = pd.DataFrame()
    
    for subreddit in subredditList:
        subredditSubs = [submission for submission in querySubmissions if submission.subreddit.display_name == subreddit]
        createdDates = [datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').date() for submission in subredditSubs]
        datesStringList = [date.strftime('%m-%d') for date in createdDates]
        
        datesColumns = list(set(datesStringList))
        datesColumns.sort()
        datesCount = Counter(datesStringList)
        datesDict = [{'Date': date, subreddit: datesCount[date]} for date in datesColumns]
        if len(datesDict) > 0:
            datesDF = pd.DataFrame(datesDict).sort_values(by='Date').set_index('Date')
            subredditDF = pd.concat([subredditDF, datesDF], axis=1)
        
    subredditDF = subredditDF.fillna(0)
    
    bokehFigure = figure()
    htmlPlot = subredditDF.plot_bokeh(kind='line', return_html=True, xlabel='Date', ylabel='# Subs', figsize=(1600, 900), legend='top_left', title=topic)
    
    bokehFigure.title.text = topic
    bokehFigure.legend.location = 'top_left'
    
    with open("Conspiracy Topics Analysis/" + topic + "_conspiracySubreddits.html" , "w") as f:
        f.write(htmlPlot)
    

def redditor_conspiracy_analysis(commonRedditorTotal, psawAPI):
    
    '''
    Taking as an argument the list of Redditors who posted a submission related to the topics mentioned in the conspiratorial_topics_analysis, this method
    studies their overall activity on Reddit, by aggregating their submissions / comments and drawing a network
    '''
    
    redditorActivity = [psawAPI.redditor_subreddit_activity(redditor) for redditor in commonRedditorTotal]
    redditorComment = [activity['comment'] for activity in redditorActivity]
    redditorSubmission = [activity['submission'] for activity in redditorActivity]
    subredditSubmissionCount = dict(Counter([subreddit for subredditList in [list(activity.keys()) for activity in redditorSubmission] for subreddit in subredditList]))
    topSubredditDict = [{'subreddit': subreddit, 'count': count} for subreddit, count in subredditSubmissionCount.items()]
    
    subredditDF = pd.DataFrame(topSubredditDict).sort_values(by='count', ascending=False).head(20)
    
    ax = subredditDF.plot(kind='barh', figsize=(20, 10), legend=False, color='coral')
    ax.set_yticklabels(subredditDF.subreddit)
    ax.set_xlabel("# Redditors")
    fig = ax.get_figure()
    fig.savefig('Conspiracy Topics Analysis/redditorsContribution.png')
    
def subreddit_contributor_extraction(subreddit, psawAPI, startEpoch=None, endEpoch=None):
    
    '''
    Method used to extract the users submitting and commenting on a specific subreddit and build a leaderboard with the top users, to see if the conversation is
    mostly lead by a handful of users or by a vast community
    '''

    if all(epoch is not None for epoch in [startEpoch, endEpoch]): 
        subredditSubmissions = psawAPI.search_submissions(before=endEpoch,
                                                          after=startEpoch,
                                                          subreddit=subreddit,
                                                          filter=['author'])

        subredditComments = psawAPI.search_comments(before=endEpoch,
                                                    after=startEpoch,
                                                    subreddit=subreddit,
                                                    filter=['author'])
    else:
        subredditSubmissions = list(psawAPI.search_submissions(subreddit=subreddit,
                                                          filter=['author']))

        subredditComments = list(psawAPI.search_comments(subreddit=subreddit,
                                                    filter=['author']))
        
    print(len(list(subredditSubmissions)))
    submissionCount = dict(Counter([submission.author.name for submission in subredditSubmissions if submission.author is not None]))
    commentCount = dict(Counter([comment.author.name for comment in subredditComments if comment.author is not None]))
    usernameList = list(submissionCount.keys())
    usernameList.extend(list(commentCount.keys()))
    usernameList = list(set(usernameList))
    authorCount = [{'Submissions': submissionCount[username], 'Comments': commentCount[username], 'Sum': submissionCount[username] + commentCount[username]} 
                   if all(username in list(countDict.keys()) for countDict in [submissionCount, commentCount])
                   else {'Submissions': submissionCount[username], 'Comments': 0, 'Sum': submissionCount[username]} if username in list(submissionCount.keys())
                   else {'Submissions': 0, 'Comments': commentCount[username], 'Sum': commentCount[username]} if username in list(commentCount.keys())
                   else {'Submissions': 0, 'Comments': 0, 'Sum': 0}
                   for username in usernameList]
    
    if len(authorCount) > 0:
        authorDF = pd.DataFrame(authorCount, index=usernameList).sort_values(by='Sum', ascending=True).tail(20)
        authorDF = authorDF.drop(['Sum'], axis=1)
        authorDF['Color'] = 'cornflowerblue'
        authorDF.to_csv(subreddit + '_author.csv')
        #authorDF.loc['kjfriend2', 'Color'] = 'coral'

        fig, axes = plt.subplots(ncols=2, sharey=True)
        authorDF.plot(x='Submissions', kind='barh', figsize=(10, 5), align='center', color='cornflowerblue', zorder=10, ax=axes[1], legend=False)
        axes[0].set(title='# Submissions')
        authorDF.plot(x='Comments', kind='barh', figsize=(10, 5), align='center', color='cornflowerblue', zorder=10, ax=axes[0], legend=False)
        axes[1].set(title='# Comments')

        axes[0].invert_xaxis()
        axes[0].set(yticklabels=authorDF.index)
        axes[0].yaxis.tick_right()

        for ax in axes.flat:
            ax.margins(0.03)
            ax.grid(True)

        fig.tight_layout()
        fig.subplots_adjust(wspace=0.4)
        plt.show()
        fig.savefig('Conspiracy Topics Analysis/' + subreddit + '.png')
    
    
def china_flu_scrapping(twitterAPI, twitterSubs):
    
    '''
    Method defined to extract any tweet shared on r/China_Flu. In case the tweet is no longer accessible, instead extract the submission title
    '''
    
    twitterList, missingTwitterList = [], []

    for submission in twitterSubs:
        urlID = submission.url.split('.com/')
        if len(urlID) > 0:
            urlID = urlID[1].split('/')
            if len(urlID) > 0:
                urlID = urlID[-1]

                try:
                    tweet = twitterAPI.get_status(urlID, tweet_mode='extended')
                    tweetUser = twitterAPI.get_user(tweet.user.screen_name)
                    tweetAuthor = tweetUser.screen_name
                    tweetUsername = tweetUser.name
                    tweetFollowers = tweetUser.followers_count
                    tweetAccountAge = tweetUser.created_at
                    try:
                        accountDate = tweetAccountAge.strftime("%Y-%m-%d")
                        accountTime = tweetAccountAge.strftime("%H:%M:%S")
                    except ValueError:
                        accountDate = 'Unknown'
                        accountTime = 'Unknown'
                        pass

                    tweetText = tweet.full_text
                    tweetDateTime = tweet.created_at

                    try:
                        tweetDate = tweetDateTime.strftime("%Y-%m-%d")
                        tweetTime = tweetDateTime.strftime("%H:%M:%S")
                    except ValueError:
                        tweetDate = 'Unknown'
                        tweetTime = 'Unknown'
                        pass

                    twitterList.append({'screen_name': tweetAuthor, 'username': tweetUsername, 'followers': tweetFollowers, 
                                        'account_date': accountDate, 'account_time': accountTime, 'text': tweetText, 'tweet_date': tweetDate,
                                        'time': tweetTime, 
                                        'upvotes': submission.score, 'downvotes': int((1 - submission.upvote_ratio) * submission.score / submission.upvote_ratio)})

                except tweepy.TweepError:
                    missingTwitterList.append({'title': submission.title, 'url': submission.url, 
                                               'date': dt.datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').date().strftime('%y-%m-%d'),
                                               'time': dt.datetime.strptime(time.ctime(submission.created_utc), '%a %b %d %H:%M:%S %Y').time().strftime('%H:%M:%S')})
                    pass

    twitterDF = pd.DataFrame(twitterList)
    missingTwitterDF = pd.DataFrame(missingTwitterList)

    # Save the DataFrames to csv files
    twitterDF.to_csv('China_Flu/twitterDF.csv')
    missingTwitterDF.to_csv('China_Flu/missingTwitter.csv')
    
def cluster_analysis(psawAPI, subredditList):
    
    '''
    By analysing the activity of each subreddit, retrieve the users who submitted new content and create a network where subreddits are linked if they share a large number
    of submitters
    '''
    
    startEpoch = int(dt.datetime(2020, 3, 1).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 8).timestamp())
    commonRedditorEdge, commonRedditorList = [], []
    
    subredditSubmissions = [psawAPI.search_submissions(before=endEpoch, 
                                                      after=startEpoch,
                                                      subreddit=subreddit,
                                                      filter=['author', 'created_utc', 'title'])
                            for subreddit in subredditList]
    
    subredditUsers = [[submission.author.name for submission in submissionList if submission.author is not None] for submissionList in subredditSubmissions]
    
    for index1, subreddit1 in enumerate(subredditList):
        for index2, subreddit2 in enumerate(subredditList):
            if subreddit1 != subreddit2:
                commonRedditors = list(set(subredditUsers[index1]).intersection(subredditUsers[index2]))
                commonRedditorEdge.append([index1, index2, len(commonRedditors), commonRedditors])
                commonRedditorList.extend([user for user in commonRedditors])
                
    clusterNetwork = nx.Graph()
    clusterNetwork.add_nodes_from(subredditList)
    
    for commonRedditor in commonRedditorEdge:
        clusterNetwork.add_edge(subredditList[commonRedditor[0]], subredditList[commonRedditor[1]], weight=commonRedditor[2])
        
    # User Analysis
    commonRedditorDict = {}
    
    for user in set(commonRedditorList):
        userCount = [user in userList for userList in subredditUsers].count(True)
        commonRedditorDict[user] = userCount
        
    commonRedditorDict = sorted(commonRedditorDict.items(), key=operator.itemgetter(1), reverse=True)
    
    # Redditor network creation
    subredditNetwork = redditor_activity_network(psawAPI, set(commonRedditorList))
        
    return clusterNetwork, commonRedditorDict, subredditNetwork


def redditor_activity_network(psawAPI, redditorList, commentAnalysis=False):
    
    '''
    Provided a list of Reddit users, this method retrieves every subreddit they have been active on and build a network, where the subreddits
    where a same user has contributed to are linked together
    '''
    
    subredditNetwork = nx.Graph()
    
    for redditor in redditorList:
        subredditList = psawAPI.redditor_subreddit_activity(redditor)['submission'].keys()
        
        if commentAnalysis:
            subredditList.extend(list(psawAPI.redditor_subreddit_activity(redditor)['submission'].keys()))
        
        for subreddit1, subreddit2 in combinations(list(set(subredditList)), 2):
            if subreddit1 not in subredditNetwork.nodes():
                subredditNetwork.add_node(subreddit1, redditor='True', userWeight=1)
            else:
                subredditNetwork.nodes[subreddit1][redditor] = 'True'
                subredditNetwork.nodes[subreddit1]['userWeight'] += 1
                
            if subreddit2 not in subredditNetwork.nodes():
                subredditNetwork.add_node(subreddit2, redditor='True', userWeight=1)
            else:
                subredditNetwork.nodes[subreddit2][redditor] = 'True'
                subredditNetwork.nodes[subreddit2]['userWeight'] += 1
                
            if not subredditNetwork.has_edge(subreddit1, subreddit2):
                subredditNetwork.add_edge(subreddit1, subreddit2, weight=1)
            else:
                subredditNetwork[subreddit1][subreddit2]['weight'] += 1
    
    # Edge filtering, with any edge under the average weight being removed
    
    edgesWeights = [edge[2]['weight'] for edge in subredditNetwork.edges(data=True)]
    avgWeight = np.mean(edgesWeights)
    
    filteredEdges = [edge for edge in subredditNetwork.edges(data=True) if edge[2]['weight'] < 5 * avgWeight]
    subredditNetwork.remove_edges_from(filteredEdges)
    print("Egdes: ", len(filteredEdges), avgWeight)
    
    nodesFiltered = [node for (node, degree) in subredditNetwork.degree if degree == 0]
    subredditNetwork.remove_nodes_from(nodesFiltered)
                
    return subredditNetwork

def community_youtube_scraping(subredditList, psawAPI):
    
    '''
    By extracting the Youtube links shared on a subset of subreddits, this method creates a graph where two subreddits are linkes together if they reference the same
    Youtube channels. Node radius depends on the number of Youtube links shared
    '''
    
    startEpoch = int(dt.datetime(2020, 3, 14).timestamp())
    endEpoch = int(dt.datetime(2020, 4, 8).timestamp())
    
    youtubeDict = []
    authorChannelDict = defaultdict(list)
    authorVideoDict = defaultdict(list)
    channelNetwork = nx.Graph()
    videoNetwork = nx.Graph()
    
    columnsDF = ['channel']
    columnsDF.extend(subredditList)
    channelDF = pd.DataFrame(columns=columnsDF)
    
    columnsDF.extend(['video'])
    videoDF = pd.DataFrame(columns=columnsDF)
    
    for subreddit in subredditList:
        subredditChannels, subredditVideos = [], []
        print("Subreddit",  subreddit)
        subredditSubmissions = list(psawAPI.search_submissions(before=endEpoch,
                                                               after=startEpoch,
                                                               subreddit=subreddit,
                                                               filter=['title', 'url', 'author']))
        
        youtubeList = [submission for submission in subredditSubmissions if 'youtube.com' in submission.url]
        
        for youtubeSub in youtubeList:
            try:
                youtubeRequest = requests.get(youtubeSub.url)
                youtubeSoup = bs(youtubeRequest.text, 'html.parser')

                youtubeTitle = youtubeSoup.find('meta', attrs={'itemprop' : 'name'})
                if youtubeTitle is not None:
                    youtubeTitle = youtubeTitle['content']

                youtubeChannel = youtubeSoup.find('script', attrs={'type' : 'application/ld+json'})
                if youtubeChannel is not None:
                    youtubeChannel = youtubeChannel.text.split('"name":')[1].split('\n')[0].strip().replace('"', '')
                    
                    # Node added to the channel network
                    if youtubeChannel not in channelNetwork.nodes():
                        channelNetwork.add_node(youtubeChannel, mentionCount = 1)
                        channelNetwork.nodes[youtubeChannel][subreddit] = True
                        authorChannelDict[youtubeChannel] = [youtubeSub.author]
                    else:
                        channelNetwork.nodes[youtubeChannel][subreddit] = True
                        channelNetwork.nodes[youtubeChannel]['mentionCount'] += 1
                        authorChannelDict[youtubeChannel].append(youtubeSub.author)
                        
                    # Node added to the video network
                    if youtubeTitle not in videoNetwork.nodes():
                        videoNetwork.add_node(youtubeTitle, channel=youtubeChannel)
                        videoNetwork.nodes[youtubeTitle]['subreddit'] = subreddit
                        authorVideoDict[youtubeTitle] = [youtubeSub.author]
                        
                        if any(covidWord in youtubeTitle.lower() for covidWord in ['coronavirus', 'covid']):
                            videoNetwork.nodes[youtubeTitle]['covid'] = True
                        else:
                            videoNetwork.nodes[youtubeTitle]['covid'] = False
                            
                    else:
                        authorVideoDict[youtubeTitle].append(youtubeSub.author)
                        
                    if youtubeChannel not in subredditChannels:
                        subredditChannels.append(youtubeChannel)
                    
                    # Channel added to the channel dataframe
                    if youtubeChannel not in list(channelDF.channel.values):
                        channelDF = channelDF.append({'channel': youtubeChannel, subreddit: True}, ignore_index=True)
                    else:
                        channelDF.loc[channelDF['channel'] == youtubeChannel, [subreddit]] = True
                        
                    # Video added to the video dataframe
                    if youtubeTitle not in list(videoDF.video.values):
                        videoDF = videoDF.append({'video': youtubeTitle, 'channel': youtubeChannel, subreddit: True,
                                                  'coronavirus': any(covidWord in youtubeTitle.lower() for covidWord in ['coronavirus', 'covid'])}, ignore_index=True)
                    else:
                        videoDF.loc[videoDF['video'] == youtubeTitle, [subreddit]] = True

                if all(youtubeAttr is not None for youtubeAttr in [youtubeTitle, youtubeChannel]):
                    youtubeDict.append({'title': youtubeTitle, 'channel': youtubeChannel, 'subreddit': subreddit})
                    
            except (ConnectTimeout, HTTPError, ReadTimeout, Timeout, ConnectionError):
                print("Error")
                pass

        # Network edge creation
        for channel1, channel2 in combinations(subredditChannels, 2):
            if channel1 != channel2:
                if not channelNetwork.has_edge(channel1, channel2):
                    channelNetwork.add_edge(channel1, channel2, weight=1)
                else:
                    channelNetwork[channel1][channel2]['weight'] += 1
                    
        for video1, video2 in combinations(subredditVideos, 2):
            if video1 != video2:
                if not videoNetwork.has_edge(video1, video2):
                    videoNetwork.add_edge(video1, video2, weight=2)
                else:
                    videoNetwork[video1][video2]['weight'] += 2
            
            
    youtubeDF = pd.DataFrame(youtubeDict)
    
    # Channel and Video DataFrames formatting
    
    channelDF = channelDF.fillna(False)   
    countList = [list(row.values).count(True) for index, row in channelDF.iterrows()]
    channelDF['count'] = countList
    channelDF = channelDF.sort_values(by='count', ascending=False).reset_index(drop=True)
    
    channelDF['userCount'] = pd.Series()
    for channel, userList in authorChannelDict.items():
        channelDF.loc[channelDF['channel'] == channel, ['userCount']] = len(list(set(userList)))
        
    channelDF['mentionCount'] = pd.Series()
    for channel in channelNetwork.nodes(data=False):
        channelDF.loc[channelDF['channel'] == channel, ['mentionCount']] = channelNetwork.nodes[channel]['mentionCount']
    
    videoDF = videoDF.fillna(False)   
    countList = [list(row.values).count(True) for index, row in videoDF.iterrows()]
    videoDF['count'] = countList
    videoDF = videoDF.sort_values(by='count', ascending=False).reset_index(drop=True)
    
    videoDF['userCount'] = pd.Series()
    for video, userList in authorVideoDict.items():
        videoDF.loc[videoDF['video'] == video, ['userCount']] = len(list(set(userList)))
    
    # Video Network: Edge creation between videos from the same channel

    for video1, video2 in combinations(videoNetwork.nodes(data=False), 2):
        if videoNetwork.nodes[video1]['channel'] == videoNetwork.nodes[video2]['channel']:
            if not videoNetwork.has_edge(video1, video2):
                videoNetwork.add_edge(video1, video2, weight=1)
            else:
                videoNetwork[video1][video2]['weight'] += 1
    
    # Community detection
    
    partition = community.best_partition(videoNetwork)
    nx.set_node_attributes(videoNetwork, name='community', values=partition)
    
    partition = community.best_partition(channelNetwork)
    nx.set_node_attributes(channelNetwork, name='community', values=partition)
    
    # Channel count retrieval for the networks
    
    for index, row in channelDF.iterrows():
        channelNetwork.nodes[row.channel]['count'] = row['count']
        channelNetwork.nodes[row.channel]['userCount'] = row['userCount']
        
    for index, row in videoDF.iterrows():
        videoNetwork.nodes[row.video]['count'] = row['count']
        videoNetwork.nodes[row.video]['userCount'] = row['userCount']
    
        
    # Edge and node filtering
    
    # CHANNEL NETWORK
    '''
    
    weightsChannel = [edge[2]['weight'] for edge in channelNetwork.edges(data=True)]
    avgChannel = np.mean(weightsChannel)
    
    filteredEdges = [edge for edge in channelNetwork.edges(data=True) if edge[2]['weight'] < avgChannel]
    channelNetwork.remove_edges_from(filteredEdges)
    print("Egdes: ", len(filteredEdges), avgChannel)
    
    nodesFiltered = [node for (node, degree) in channelNetwork.degree if degree == 0]
    channelNetwork.remove_nodes_from(nodesFiltered)
    
    '''
    
    # VIDEO NETWORK
    
    '''
    weightsVideo = [edge[2]['weight'] for edge in videoNetwork.edges(data=True)]
    avgVideo = np.mean(weightsVideo)
    
    filteredEdges = [edge for edge in videoNetwork.edges(data=True) if edge[2]['weight'] < 4 * avgVideo]
    videoNetwork.remove_edges_from(filteredEdges)
    print("Egdes: ", len(filteredEdges), avgVideo)
    
    nodesFiltered = [node for (node, degree) in videoNetwork.degree if degree == 0]
    videoNetwork.remove_nodes_from(nodesFiltered)
    '''
    
    return youtubeDF, channelNetwork, channelDF, videoNetwork, videoDF, authorChannelDict


def youtube_feed_scraping(psawAPI):
    
    '''
    Method used to retrieve every Youtube channel followed by u/thefeedbot
    '''
    
    startEpoch = int(dt.datetime(2019, 1, 1).timestamp())
    endPoch = int(dt.datetime(2019, 12, 31).timestamp())
    
    firstTime, lastTime = {}, {}
    channelCount, covidCount, chloroquineCount, fiveGCount = {}, {}, {}, {}
    feedSubmissions = list(psawAPI.search_submissions(after=startEpoch,
                                                      subreddit='TheVideoFeed',
                                                      filter=['url', 'title', 'created_utc']))
    for submission in feedSubmissions:
        try:
            youtubeRequest = requests.get(submission.url)
            youtubeSoup = bs(youtubeRequest.text, 'html.parser')

            youtubeTitle = youtubeSoup.find('meta', attrs={'itemprop' : 'name'})
            if youtubeTitle is not None:
                youtubeTitle = youtubeTitle['content']

            youtubeChannel = youtubeSoup.find('script', attrs={'type' : 'application/ld+json'})
            if youtubeChannel is not None:
                youtubeChannel = youtubeChannel.text.split('"name":')[1].split('\n')[0].strip().replace('"', '')
            
            if all(youtubeInfo is not None for youtubeInfo in [youtubeTitle, youtubeChannel]):
                if youtubeChannel not in firstTime.keys():
                    firstTime[youtubeChannel] = submission.created_utc
                else:
                    if submission.created_utc < firstTime[youtubeChannel]:
                        firstTime[youtubeChannel] = submission.created_utc

                if youtubeChannel not in lastTime.keys():
                    lastTime[youtubeChannel] = submission.created_utc
                else:
                    if submission.created_utc > lastTime[youtubeChannel]:
                        lastTime[youtubeChannel] = submission.created_utc

                if youtubeChannel not in channelCount.keys():
                    channelCount[youtubeChannel] = 1
                else:
                    channelCount[youtubeChannel] += 1

                if any(covidWord in youtubeTitle.lower() for covidWord in ['coronavirus', 'covid']):
                    if youtubeChannel not in covidCount.keys():
                        covidCount[youtubeChannel] = 1
                    else:
                        covidCount[youtubeChannel] += 1
                else:
                    if youtubeChannel not in covidCount.keys():
                        covidCount[youtubeChannel] = 0

                if 'chloroquine' in youtubeTitle.lower():
                    if youtubeChannel not in chloroquineCount.keys():
                        chloroquineCount[youtubeChannel] = 1
                    else:
                        chloroquineCount[youtubeChannel] += 1
                else:
                    if youtubeChannel not in chloroquineCount.keys():
                        chloroquineCount[youtubeChannel] = 0

                if '5g' in youtubeTitle.lower():
                    if youtubeChannel not in fiveGCount.keys():
                        fiveGCount[youtubeChannel] = 1
                    else:
                        fiveGCount[youtubeChannel] += 1   
                else:
                    if youtubeChannel not in fiveGCount.keys():
                        fiveGCount[youtubeChannel] = 0
                
        except (ConnectTimeout, HTTPError, ReadTimeout, Timeout, ConnectionError):
            print("Error")
            pass
        
    youtubeFeedDict = [{'channel': channel, 'count': channelCount[channel], 'covidCount': covidCount[channel], 'chloroquineCount': chloroquineCount[channel],
                        '5GCount': fiveGCount[channel], 
                        'firstDate': datetime.strptime(time.ctime(firstTime[channel]), '%a %b %d %H:%M:%S %Y').date().strftime('%d-%m-%y'), 
                        'lastDate': datetime.strptime(time.ctime(lastTime[channel]), '%a %b %d %H:%M:%S %Y').date().strftime('%d-%m-%y')} 
                        for channel in channelCount.keys()]
    
    youtubeFeedDF = pd.DataFrame(youtubeFeedDict)
    return youtubeFeedDF
        
def named_entity_recognition(subreddit, psawAPI):
    
    '''
    Method used to retrieve the name of people mentioned in a subreddit
    '''
    
    nlp = spacy.load("en")
    
    
    subredditTitles = [submission.title for submission in list(psawAPI.search_submissions(subreddit=subreddit, filter=['url', 'title', 'author']))]
    #entityTags = [nlp(title) for title in subredditTitles]
    
    #print([token for token in entityTags[0].ents])
    for title in subredditTitles:
        print("Title:", title)
        tags = nlp(title)
        ents = [(e.text, e.start_char, e.end_char, e.label_) for e in tags.ents]
        print(ents)
        
    return [token for entityList in entityTags for token in entityList.ents if token.ent_type_ == 'PERSON']