# Module for #sad project

import pandas as pd
import os
import json
from datetime import datetime, timedelta, date
from twython import TwythonError
import preprocessor as tweetpre
from collections import Counter
import numpy as np
import datetime as dt
import time

import pycountry
import pycountry_convert as pc

import plotly.graph_objects as go
import plotly.express as px

import networkx as nx
from networkx.algorithms.approximation.clustering_coefficient import average_clustering

import community

import praw

from nltk import word_tokenize
from nltk.corpus import stopwords
from string import punctuation

from random import sample
import matplotlib.colors as mcolors

from sklearn.feature_extraction.text import TfidfVectorizer

from tqdm import tqdm
import matplotlib.pyplot as plt

import plotly.graph_objects as go
import plotly.express as px

from prawcore.exceptions import Forbidden, NotFound

def subreddit_graph_creation(reddit, subreddit, graphDepth):
    
    redditorGraph = nx.Graph()
    
    redditorProcessed = []
    #redditorList = [submission.author.name for submission in reddit.subreddit(subreddit).top(limit=5)]
    for submission in reddit.subreddit('worldnews').search('Global COVID-19 Pandemic'):
    #for submission in reddit.subreddit('Switzerland').search('[Megathread] Coronavirus'):
    #for submission in reddit.subreddit('askscience').search('Coronavirus Megathread'):
    #for submission in reddit.subreddit('Anxiety').search('Coronavirus'):
    #for submission in reddit.subreddit('news').search('COVID-19 Megathread'):
        userList = [comment.author.name for comment in submission.comments 
                    if type(comment) is praw.models.reddit.comment.Comment and comment.author is not None]
    
    redditorList = userList[0:5]
    redditorGraph.add_nodes_from(redditorList, nodeType='user')
    
    for depth in range(graphDepth):
        redditorProcessed.extend(redditorList)
        print("New depth: ", depth, len(redditorProcessed))
        newRedditor = [] # List used to store new users added to the graph
        for redditor in redditorList:
            try:
                for submission in reddit.redditor(redditor).submissions.new(limit=5):
                    
                    if submission.subreddit.display_name not in redditorGraph.nodes():
                        redditorGraph.add_node(submission.subreddit.display_name, nodeType='subreddit',
                                               description=submission.subreddit.description)
                        if ('covid' in submission.subreddit.description.lower() or 
                           'coronavirus' in submission.subreddit.description.lower()):
                            redditorGraph.nodes[submission.subreddit.display_name]['sub_type'] ='covid'
                        else:
                            redditorGraph.nodes[submission.subreddit.display_name]['sub_type'] = 'non-covid'
                        
                        
                    if not redditorGraph.has_edge(redditor, submission.subreddit.display_name):
                        redditorGraph.add_edge(redditor, submission.subreddit.display_name, weight=1,
                                               subreddit=submission.subreddit.display_name,
                                               comment=submission.subreddit.description)
                        if redditorGraph.nodes[submission.subreddit.display_name]['sub_type'] == 'covid':
                            redditorGraph.nodes[redditor]['sub_type'] = 'covid_user'
                    else:
                        redditorGraph[redditor][submission.subreddit.display_name]['weight'] += 1
                        
                    for comment in submission.comments:
                        if type(comment) is praw.models.reddit.comment.Comment and comment.author is not None:
                            if comment.author not in redditorGraph.nodes():
                                redditorGraph.add_node(comment.author.name, nodeType='user')

                            if not redditorGraph.has_edge(redditor, comment.author):
                                redditorGraph.add_edge(redditor, comment.author.name, weight=1, 
                                                       subreddit=submission.subreddit.display_name,
                                                       comment=comment.body)
                            else:
                                redditorGraph[redditor][comment.author.name]['weight'] += 1   
                                
                            if comment.author.name not in redditorProcessed:
                                newRedditor.append(comment.author.name)
                                
                            if not redditorGraph.has_edge(comment.author.name, submission.subreddit.display_name):
                                redditorGraph.add_edge(comment.author.name, submission.subreddit.display_name, weight=1,
                                                       subreddit=submission.subreddit.display_name,
                                                       comment=submission.subreddit.description)
                                if redditorGraph.nodes[submission.subreddit.display_name]['sub_type'] == 'covid':
                                    redditorGraph.nodes[comment.author.name]['sub_type'] = 'covid_user'
                            else:
                                redditorGraph[comment.author.name][submission.subreddit.display_name]['weight'] += 1
                                                
            except Forbidden:
                print("Forbidden")
                
        # Creating the list with the new users to process
        redditorList = newRedditor
            
    # Community detection
    
    partition = community.best_partition(redditorGraph)
    
    nx.set_node_attributes(redditorGraph, name='community', values=partition)
    
    community_analysis(redditorGraph, partition)
    
    for node in redditorGraph.nodes():
        if redditorGraph.nodes[node]['nodeType'] == 'subreddit':
            redditorGraph.nodes[node]['community'] = len(partition)
            redditorGraph.nodes[node]['nodeGroup'] = node
        else:
            subredditEdgeList = [edge for edge in redditorGraph.edges(data=True) 
                             if node in [edge[0], edge[1]] and 'subreddit' not in edge[2].keys()]
            
            if len(subredditEdgeList) > 0:
                majorSubreddit = subredditEdgeList[np.argmax([edge[2]['weight'] for edge in subredditEdgeList])][1]
                redditorGraph.nodes[node]['nodeGroup'] = majorSubreddit
            
            
    # Generate edge list in Pandas DataFrame
    
    edgeList = [{'node1': edge[0], 'node2': edge[1], 'weight': edge[2]['weight'], 
                 'subreddit': edge[2]['subreddit'], 'comment': edge[2]['comment']}
                for edge in redditorGraph.edges(data=True) 
                if redditorGraph.nodes[edge[0]]['nodeType'] != 'subreddit'
                and redditorGraph.nodes[edge[1]]['nodeType'] != 'subreddit']
    
    edgeDF = pd.DataFrame(edgeList)
    print(edgeDF)
    
    edgeDF.to_csv('edgeDF.csv', index=False)
    return redditorGraph

def community_analysis(redditorGraph, partition):
    
    communityDict = []
    
    #print(len(redditorGraph.nodes()))
    for community in set(partition.values()):
        communityNodes = [node for node in redditorGraph.nodes() if redditorGraph.nodes[node]['community'] == community]
        
        communityGraph = redditorGraph.subgraph(communityNodes)
        
        # Indicators computation
        
        clusteringCoeff = nx.algorithms.approximation.clustering_coefficient.average_clustering(communityGraph)
        communityDict.append({'community_id': community, 
                         '# nodes': len(communityGraph.nodes()),
                         'clustering_coeff': clusteringCoeff, 
                         'subreddits': [node for node in communityGraph.nodes() 
                                        if communityGraph.nodes[node]['nodeType'] == 'subreddit']})
    
    #print(communityDict)
    communityDF = pd.DataFrame(communityDict)
    print(communityDF)
    communityDF.to_csv('communityDF.csv')
    
def covid_subreddit_tracking(reddit, date):
    
    '''
    Method used to track the activity on the Coronavirus subreddit (r/Coronavirus) in order to assess the news 
    related by the users
    
    Main points:
    - Sort the news by country / region
    - Extract the sources linked by the users
    - Export the data in a .csv file
    '''
            
    europeList = [submission for submission in reddit.subreddit('Coronavirus').new(limit=2000) 
                 if dt.datetime.fromtimestamp(submission.created_utc).replace(hour=0, minute=0, second=0, microsecond=0)
                 == dt.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0) 
                 and submission.link_flair_text == 'Europe']
    
    usaList = [submission for submission in reddit.subreddit('Coronavirus').new(limit=100) 
                 if dt.datetime.fromtimestamp(submission.created_utc).replace(hour=0, minute=0, second=0, microsecond=0)
                 == dt.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0) 
                 and submission.link_flair_text == 'USA']
    
    asiaList = [submission for submission in reddit.subreddit('Coronavirus').new(limit=100) 
                 if dt.datetime.fromtimestamp(submission.created_utc).replace(hour=0, minute=0, second=0, microsecond=0)
                 == dt.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0) 
                 and submission.link_flair_text == 'Central & East Asia']
    
    print(len(europeList), len(usaList), len(asiaList))
    
    swissList = [submission for submission in europeList if 'Switzerland' in submission.title 
                                                         or 'Switzerland' in submission.selftext]
    
    print(len(swissList))
    
    for swissSub in swissList:
        print(swissSub.title, swissSub.url)
        
    europeCountry = {}
    for europeSub in europeList:
        country = [country.name for country in pycountry.countries if country.name in europeSub.title]
        if len(country) > 0:
            if country[0] not in europeCountry.keys():
                europeCountry[country[0]] = 1
            else:
                europeCountry[country[0]] += 1
                
    europeList = [{'country': country, 'count': count, 'iso_alpha': pc.country_name_to_country_alpha3(country)} 
                  for country, count in europeCountry.items()]
                
    print(europeCountry)
    europeDF = pd.DataFrame(europeList)
    print(europeDF)
    
    for index, row in europeDF.iterrows():
        row['iso_alpha'] = pc.country_name_to_country_alpha3(row.country)
    
    fig = go.Figure(go.Scattergeo())
    fig.update_geos(projection_type="natural earth")
    fig.update_layout(height=300, margin={"r":0,"t":0,"l":0,"b":0})
    fig = px.choropleth(europeDF, locations="iso_alpha",
                    color="count", # lifeExp is a column of gapminder
                    hover_name="country", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.Plasma)
    fig.show()

        
        
        
        
                
            


    

